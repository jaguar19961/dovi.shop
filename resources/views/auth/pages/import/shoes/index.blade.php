@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Shoes</h3>
                    <div class="d-flex">
                        <a href="{{route('admin.shoes.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>
                        <a href="{{route('admin.shoes.deleteall')}}" class="btn btn-sm btn-danger  mr-4 ">Delete all shoes</a>
                        @if(Route::currentRouteName() === 'admin.shoes.out')
                            <a href="{{route('admin.shoes.index')}}" class="btn btn-sm btn-info mr-4">All</a>
                        @else
                            <a href="{{route('admin.shoes.out')}}" class="btn btn-sm btn-info mr-4">Show out of stock</a>

                        @endif
                    </div>
                    {{--                    <form class="" action="">--}}
                    {{--                        <div class="col-lg-3">--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <label for="exampleFormControlSelect1">Manufacture</label>--}}
                    {{--                                <select class="form-control" name="furnizor_id" id="exampleFormControlSelect1">--}}
                    {{--                                    <option value="0" disabled selected>Select manufacture</option>--}}
                    {{--                                    @foreach($brands as $item)--}}
                    {{--                                        <option value="{{$item->id}}">{{$item->name}}</option>--}}
                    {{--                                    @endforeach--}}
                    {{--                                </select>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-lg-3">--}}
                    {{--                            <button class="btn btn-secondary">Delete by furnizor</button>--}}
                    {{--                        </div>--}}
                    {{--                    </form>--}}
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Name</th>
                            <th scope="col" class="sort" data-sort="name">Download</th>
                            <th scope="col" class="sort" data-sort="name">SKU</th>
                            <th scope="col" class="sort" data-sort="budget">Price</th>
                            <th scope="col" class="sort" data-sort="budget">Category</th>
                            <th scope="col" class="sort" data-sort="budget">Color</th>
                            <th scope="col" class="sort" data-sort="budget">Size</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="status">Status</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                            <tr>
                                <td class="budget">
                                    <a target="_blank" href="/shoes/{{$item->slug}}">{{$item->slug}}</a>
                                </td>
                                <td class="budget">
                                    <a target="_blank"
                                       href="{{url('/admin/shoes/downloadImage/'.$item->id)}}">Download</a>
                                </td>
                                <td class="budget">
                                    # {{$item->sku}}
                                </td>
                                <td class="budget">
                                    {{--                                    {{$item->price}} {{$item->valute_id}}--}}
                                </td>
                                <td class="budget">
                                    {{$item->category ? $item->category->name : 'no'}}
                                </td>
                                <td class="budget">
                                    {{$item->color ? $item->color->name : 'no'}}
                                </td>
                                <td class="budget">
                                    @if($item->size != null)
                                        @if(count($item->size) > 0)
                                            @foreach($item->size as $el)
                                                {{$el}}
                                            @endforeach
                                        @endif
                                    @endif
                                </td>
                                <td class="budget">
                                    {{$item->created_at->format('d-m-Y')}}
                                </td>

                                <td class="budget">
                                    @if(count($item->size) === 0)
                                        <span class="badge badge-danger">Out of stock</span>
                                    @else
                                        <span class="badge badge-success">In stock</span>
                                    @endif
                                </td>

                                <td class="budget">
                                    {{--                                    <a href="{{route('admin.blog.show', $item->id)}}"--}}
                                    {{--                                       class="btn btn-sm btn-info  mr-4 ">Edit</a>--}}
                                    <a href="{{route('admin.shoes.destroy', $item->id)}}"
                                       class="btn btn-sm btn-danger  mr-4 ">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{$model->render()}}
        </div>
    </div>
@endsection
