@extends('auth.layouts.app')
@section('content')
    <!-- Dark table -->
    <div class="row">
        <div class="col">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0 d-flex justify-content-between">
                    <h3 class="text-white mb-0">Variables</h3>
                    <a href="{{route('admin.variable.create')}}" class="btn btn-sm btn-warning  mr-4 ">Create new</a>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-dark table-flush">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Key</th>
                            <th scope="col" class="sort" data-sort="name">Name Ro</th>
                            <th scope="col" class="sort" data-sort="name">Name Ru</th>
                            <th scope="col" class="sort" data-sort="name">Slug</th>
                            <th scope="col" class="sort" data-sort="budget">Created</th>
                            <th scope="col" class="sort" data-sort="completion">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        @foreach($model as $item)
                            <tr>
                                <td class="budget">
                                    {{$item->key}}
                                </td>
                                <td class="budget">
                                    {{substr($item->name_ro, 0, 150)}}
                                </td>
                                <td class="budget">
                                    {{substr($item->name_ru, 0, 150)}}
                                </td>
                                <td class="budget">
                                    {{substr($item->slug, 0, 150)}}
                                </td>
                                <td class="budget">
                                    {{$item->created_at->format('d-m-Y')}}
                                </td>

                                <td class="budget">
                                    <a href="{{route('admin.variable.show', $item->id)}}"
                                       class="btn btn-sm btn-info  mr-4 ">Edit</a>
{{--                                    <a href="{{route('admin.variable.destroy', $item->id)}}"--}}
{{--                                       class="btn btn-sm btn-danger  mr-4 ">Delete</a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
