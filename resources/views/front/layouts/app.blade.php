<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    {!! Meta::tag('robots') !!}

    {!! Meta::tag('site_name', 'DoviShop') !!}
    {!! Meta::tag('url', Request::url()); !!}
    {!! Meta::tag('locale', 'en_EN') !!}

    {!! Meta::tag('title') !!}
    {!! Meta::tag('description') !!}

    {!! Meta::tag('canonical') !!}
    {!! Meta::tag('image', asset('images/default-logo.png')) !!}

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <script src="{{mix('js/app.js')}}" defer></script>
    <script src="https://kit.fontawesome.com/8628af49ce.js" crossorigin="anonymous"></script>
</head>
<body>
<div id="app" v-cloak>
    <div class="">
        <div class="">
            <div class="">
                <search-bar :lang="{{json_encode(app()->getLocale())}}" :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}" type="mobile" class="header__search_var" v-if="search_bar_mobile"></search-bar>
            </div>
        </div>
    </div>
   <div :class="{'main_bloor' : search_bar_mobile}">
       @include('front.ui.header')
       @yield('content')
       @include('front.ui.footer')
   </div>
    <div class="d-none">{!! $script->name !!}</div>
</div>
</body>
</html>
