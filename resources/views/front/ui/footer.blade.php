<div class="bg-white shadow fixed-bottom d-lg-none">
    <div class="container pt-2 pb-4">
        <div class="row no-gutters">
            <div class="col-2">
                <div class="text-center pt-2 pb-2" @click="$root.drawer = !$root.drawer">
                    <img src="/images/ionic-ios-menu.svg" class="h1 mb-1" height="18">
                    <span class="small d-block w-100 text-center">{{app()->getLocale() === 'ro' ? 'MENIU' : 'МЕНЮ'}}</span>
                </div>
            </div>
            <div class="col-2">
                <div class="text-center pt-2 pb-2" @click="search_bar_mobile = !search_bar_mobile">
                    <img src="/images/ionic-ios-search.svg" class="h1 mb-1 ml-n1" height="18">
                    <span class="small d-block w-100">{{app()->getLocale() === 'ro' ? 'CĂUTARE' : 'ПОИСК'}}</span>
                </div>
            </div>

            <div class="col-4">
                <a href="/catalog" class="btn-shop text-center align-content-center mx-auto mt-n4 shadow-sm">
                    {{-- feather-shopping-bag ionic-ios-arrow-back  --}}
                    {{-- <img src="/images/ionic-ios-arrow-back.svg" class="h1 mb-0 d-inline-block mx-auto" height="19">
                    <span class="small d-block w-100 text-center mt-1">BACK</span> --}}
                    <img src="/images/categories.svg" class="h1 mb-0 d-inline-block mx-auto" height="19">
                    <span class="extrasmall d-block w-100 text-center mt-1">{{app()->getLocale() === 'ro' ? 'CATALOG' : 'КАТАЛОГ'}}</span>
                    {{-- <img src="/images/feather-shopping-bag.svg" class="h1 mb-0 d-inline-block mx-auto" height="19">
                    <span class="small d-block w-100 text-center mt-1">SHOP</span> --}}
                </a>
            </div>

            <div class="col-2">
                @auth()
                    <div class="text-center pt-2 pb-2" onclick="window.location.href = '/account/my_account'">
                        <img src="/images/feather-user.svg" class="h1 mb-1" height="18">
                        <span class="small d-block w-100">{{app()->getLocale() === 'ro' ? 'CONT' : 'Аккаунт'}}</span>
                    </div>
                @else
                    <div class="text-center pt-2 pb-2" onclick="window.location.href = '/user_login'">
                        <img src="/images/feather-user.svg" class="h1 mb-1" height="18">
                        <span class="small d-block w-100">{{app()->getLocale() === 'ro' ? 'Logare' : 'Вход'}}</span>
                    </div>
                @endauth
            </div>
            <div class="col-2">
                <div class="text-center pt-2 pb-2 position-relative" onclick="window.location.href = '/cart'">
                    <count-cart-footer></count-cart-footer>
                    <img src="/images/bag.svg" class="h1 mb-1 position-relative " height="18">
                    <span class="small d-block w-100">{{app()->getLocale() === 'ro' ? 'COŞ' : 'КОРЗИНА'}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
<side-bar :lang="{{json_encode(app()->getLocale())}}"
          :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}"
          :route="{{json_encode(Route::currentRouteName())}}"
          :auth_check="{{json_encode(auth()->check())}}"></side-bar>
<section id="footer_bs" class="d-lg-block bg-white font-weight-semibold">
    <div class="container border-top">
        <div class="row pt-3 pt-lg-5 mb-4 mb-lg-0 pb-5 pb-lg-0">
            <div class="col-lg-3  pr-3 order-2 pt-4">
                <div class="row no-gutters py-2 mb-3">
                    <div class="col-12 px-0"><span class="normal">{{app()->getLocale() === 'ro' ? 'Aboneaza-te la newsletter-ul nostru' : 'Подписывайтесь на нашу новостную рассылку'}}</span></div>
                </div>
                <el-input class="rounded-pill overflow-hidden w-100 el-input-group--catskill-white"
                          placeholder="EMAIL">
                    <template slot="append"><i class="el-icon-right font-weight-bold"></i></template>
                </el-input>
            </div>
            <div class="col-lg-3 order-3 pl-lg-0 pt-4">
                <div class="row no-gutters py-2">
                    <div class="col-6 px-0"><span class="text-gray-400 normal"><a href="mail:info@dovi.shop">info@dovi.shop</a></span></div>
                    <div class="col-6 px-0"><span class="text-gray-400 normal">Chișinău, Moldova</span></div>
                </div>
                <div class="row no-gutters py-2">
                    <div class="col-6 px-0"><span class="text-gray-400 normal"><a href="tel:+37367744799">+(373) 67 744 799</a></span></div>
                    {{--                    <div class="col-6 px-0"><span class="text-gray-400 normal">str. Ștefan cel Mare 32</span></div>--}}
                </div>
            </div>
            <div class="col-lg-3 order-4 pt-4">
                <div class="row py-2">
                    <div class="col-4"><a href="https://www.facebook.com/dovishop.fashion" class="normal text-gray-400 hover-primary">Facebook</a></div>
                    <div class="col-4"><a href="https://www.instagram.com/dovi.shop_fashion/" class="normal text-gray-400 hover-primary">Instagram</a></div>
                    <div class="col-4"><a href="https://wa.me/+37367744799" class="normal text-gray-400 hover-primary">Whatsapp</a></div>
                </div>
                <div class="row py-2">
{{--                    <div class="col-4"><a href="https://www.tiktok.com/@dovi.shop" class="normal text-gray-400 hover-primary">TIKTOK</a></div>--}}
                </div>
            </div>
            <div class="col-lg-3 order-5 order-lg-1 text-center text-lg-left pt-4 pb-5 pt-lg-0">
                <img class="d-none d-lg-block mt-4" src="/images/logo/logo_black.svg" height="45"/>
                <img class="d-lg-none" src="/images/logo/logo_black.svg" height="73"/>
                <p class="text-gray-400 mt-4 normal">© 2021 DOVI.SHOP Toate drepturile rezervate.
                    Developed with love by <a href="https://midavco.com">Midavco.</a></p>
            </div>
        </div>
    </div>
</section>

