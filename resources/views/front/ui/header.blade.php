<header id="header" class="d-lg-block">
    <div class="container">
        <div class="row flex-grow-1">
            <div class="menu d-none d-lg-block align-self-center col-auto order-lg-2" :class="search_bar ? 'flex-grow-1 pr-0 mr-0 ml-auto' : 'mx-auto'">
                <search-bar :lang="{{json_encode(app()->getLocale())}}" :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}" type="descktop" class="header__search_var" v-if="search_bar"></search-bar>
                <ul class="pl-0" :class="{'d-none' : search_bar == true}">
                    <li>
                        <a href="/" class="menu_item @if(Route::currentRouteName() === 'index') active @endif ">{{vars('839518')}}</a>
                    </li>
                    <li>
                        <a href="/catalog"
                        class="menu_item @if(Route::currentRouteName() === 'catalog') active @endif">{{vars('660799')}}</a>
                    </li>
                    <li>
                        <a href="/about-us" class="menu_item @if(Route::currentRouteName() === 'aboutUs') active @endif">{{vars('928026')}}</a>
                    </li>
                    <li>
                        <a href="/contact" class="menu_item @if(Route::currentRouteName() === 'contact') active @endif">{{vars('954767')}}</a>
                    </li>
                </ul>
            </div>
            <div class="action_menu flex-grow-1 flex-lg-grow-0 ml-lg-auto col-auto order-lg-3 align-self-center" :class="{'pl-0' : search_bar == true}">
                <div class="search d-none"  :class="search_bar ? 'd-lg-none' : 'd-lg-block'"  @click="search_bar = !search_bar">
                    <i class="el-icon-search"></i>
                </div>
                <div class="search d-none" :class="search_bar ? 'd-lg-block' : 'd-lg-none'"></div>
                <el-dropdown class="menu_lang order-3 order-lg-1 ml-auto">
                    <span class="el-dropdown-link text-uppercase">
                        {{LaravelLocalization::getCurrentLocale()}}<i class="el-icon-arrow-down el-icon--right"></i>
                    </span>
                    <el-dropdown-menu slot="dropdown">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <el-dropdown-item>
                                <a rel="alternate" class="text-uppercase" hreflang="{{ $localeCode }}"
                                href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    {{ $localeCode }}
                                </a>
                            </el-dropdown-item>
                        @endforeach
                    </el-dropdown-menu>
                </el-dropdown>
                <div class="order-2 d-lg-none mx-auto">
                    <i class="fas fa-phone-alt mr-1"></i>
                    <span class="font-weight-bold normal pr-4"><a href="tel: +(373) 67 744 799">+(373) 67 744 799</a></span>
                </div>
                <el-dropdown class="menu_valute order-1 order-lg-2 mr-auto">
                    <span class="el-dropdown-link text-uppercase">
                        {{\Illuminate\Support\Facades\Session::get('valute')}}<i
                            class="el-icon-arrow-down el-icon--right"></i>
                    </span>
                    <el-dropdown-menu slot="dropdown">
                        @foreach(valutes() as $valute)
                            <el-dropdown-item>
                                <a href="{{url('/set-valute')}}"
                                onclick="event.preventDefault(); document.getElementById('submit-valute-{{$valute->code}}').submit();">
                                    {{$valute->name}}
                                </a>
                                <form id="submit-valute-{{$valute->code}}" action="{{ route('valute') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                    <input type="hidden" name="valute" value="{{$valute->code}}"/>
                                </form>
                            </el-dropdown-item>
                        @endforeach
                    </el-dropdown-menu>
                </el-dropdown>
                @auth()
                    <el-dropdown class="menu_account d-none d-lg-inline-block order-lg-3">
                    <span class="el-dropdown-link text-uppercase">
                        Account<i class="el-icon-arrow-down el-icon--right"></i>
                    </span>
                        <el-dropdown-menu slot="dropdown">
                            <el-dropdown-item onclick="window.location.href = '/account/my_account'">{{vars('993416')}}
                            </el-dropdown-item>
                            <el-dropdown-item onclick="window.location.href = '/account/orders'">{{vars('795210')}}</el-dropdown-item>
                            <el-dropdown-item onclick="window.location.href = '/account/whishlist'">{{vars('739988')}}</el-dropdown-item>
                        </el-dropdown-menu>
                    </el-dropdown>
                @else
                    <a class="text-black hover-primary font-weight-bold text-decoration-none text-uppercase el-dropdown ml-3 d-none d-lg-inline-block order-lg-3"  href="/user_login">
                        <span class="el-dropdown-link">{{vars('199346')}}</span>
                    </a>
                @endauth


            </div>
            <div class="w-100 d-lg-none border-top border-light my-4"></div>
            <div class="left_menu flex-grow-1 flex-lg-grow-0 pr-0 ml-lg-0 mr-lg-auto col-auto order-lg-1">
                <div class="burger d-lg-none" @click="$root.drawer = !$root.drawer">
                    <div class="b_item"></div>
                    <div class="b_item"></div>
                    <div class="b_item"></div>
                </div>


                <div class="logo mx-auto mx-lg-0">
                    <a href="/"><img class="d-none d-lg-block" src="/images/logo/logo_black.svg" height="77" width="122" /></a>

                    <a href="/"><img class="d-lg-none" src="/images/logo/logo_black.svg" height="55" width="122" /></a>
                </div>

            </div>
            <div class="pl-0 ml-lg-4 align-self-center col-auto order-lg-4">
                <count-cart/>
            </div>
        </div>
    </div>
</header>
