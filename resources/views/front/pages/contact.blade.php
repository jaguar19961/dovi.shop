@extends('front.layouts.app')
@section('content')
    <section id="contact">
        <div class="container">
            <div class="row py-5">
                <div class="row col-12 align-items-center text-center mx-auto mb-5 pb-lg-5">
                    <h1 class="col-auto col-lg-12 font-weight-bold px-0  mr-1 mr-lg-0 mb-0 mb-lg-2 d-xsm-none">{{app()->getLocale() === 'ro' ? 'Contacte' : 'Контакты'}}</h1>
                    <div class="col-auto col-lg-12 breadcrumb justify-content-center mb-0 mr-auto">
                        <ul class="breadcrumb_list mb-0">
                            <li class="breadcrumb_list_item">
                                <a href="/" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Pagina principala' : 'Главная'}}</a>
                            </li>
                            <li class="breadcrumb_list_item active mr-0">
                                <a href="/contact" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Contacte' : 'Контакты'}}</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="offset-3 col-6 col-lg-5 offset-lg-0 text-center align-self-center">
                    <img src="/images/logo/logo_black.svg" width="300px"/>
                </div>
                <div class="col-lg-1 justify-content-center my-3 align-self-center">
                    <div class="v-divider d-none d-lg-block my-3"></div>
                    <div class="h-divider d-lg-none mb-4 mt-3 mx-auto"></div>
                </div>
                <div class="col-lg-5">
                    <h3 class="font-weight-bold text-center text-lg-left">
                        Chișinău, Moldova
                        {{--                        <br/> str. Ștefan cel Mare 32--}}
                    </h3>
                    <div class="my-4 ">
                        <div class="font-weight-bold row justify-content-center justify-content-lg-start"><span
                                class="text-gray-400 col-auto col-lg-2">{{vars('420947')}}:</span> <span>info@dovi.shop</span></div>
                        <div class="font-weight-bold row justify-content-center justify-content-lg-start"><span
                                class="text-gray-400 col-auto  col-lg-2">{{vars('676571')}}:</span> <span><a
                                    href="tel:+(373) 67 744 799">+(373) 67 744 799</a></span>
                        </div>
                    </div>
                    <div class="font-weight-600">
                        <div class="row py-1">
                            <a class="col-4 text-gray-400" href="https://www.facebook.com/dovishop.fashion">Facebook</a>
                            <a class="col-4 text-gray-400" href="https://www.instagram.com/dovi.shop_fashion/">Instagram</a>
                            <a class="col-4 text-gray-400" href="https://wa.me/+37367744799">Whatsapp</a>
                        </div>
                        <div class="row py-1">
{{--                            <a class="col-4 text-gray-400" href="https://www.tiktok.com/@dovi.shop">TikTok</a>--}}
{{--                            <a class="col-4 text-gray-400" href="#">Twitter</a>--}}
{{--                            <a class="col-4 text-gray-400" href="#">Whatsapp</a>--}}
                        </div>
                    </div>

                </div>
                <div class="col-lg-1"></div>
            </div>
            {{--            <div class="row mx-auto my-5 bg-rect">--}}
            {{--                <iframe class="h-100 w-100" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2720.2590551060807!2d28.844186940509022!3d47.01552093659302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c97c3ccd6382c9%3A0xb28916c3bd5678b3!2zQnVsZXZhcmR1bCDImHRlZmFuIGNlbCBNYXJlIMiZaSBTZsOubnQgMzIsIENoaciZaW7Eg3UsIE1vbGRvdmE!5e0!3m2!1sro!2sro!4v1611349582720!5m2!1sro!2sro" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>--}}
            {{--            </div>--}}
            <div class="row py-5">
                <div class="offset-lg-3 col-lg-6 text-center">
                    <h2 class="font-weight-bold">{{vars('878859')}}</h2>
{{--                    <p class="font-italic text-secondary">{{vars('289901')}}</p>--}}
                </div>
            </div>

            <form>
                <div class="row py-3">
                    <div class="col-lg-4"><input type="text"
                                                 class="form-control form-control-flush border-bottom rounded-0 w-100"
                                                 placeholder="{{vars('951856')}}"/></div>
                    <div class="col-lg-4"><input type="text"
                                                 class="form-control form-control-flush border-bottom rounded-0  w-100"
                                                 placeholder="{{vars('420947')}}"/></div>
                    <div class="col-lg-4"><input type="text"
                                                 class="form-control form-control-flush border-bottom rounded-0  w-100"
                                                 placeholder="{{vars('138759')}}"/></div>
                </div>
                <div class="row py-3">
                    <div class="col-12">
                        <textarea class="form-control form-control-flush w-100 rounded-0  border-bottom"
                                  style="height: 200px" placeholder="{{vars('447912')}}"></textarea>
                    </div>
                </div>
                <div class="row justify-content-center my-5">
                    <button type="button" class="btn btn-lg rounded-pill text-white  font-weight-bold btn-primary">
                        {{vars('749098')}}</button>
                </div>
            </form>

        </div>
    </section>
@endsection
