@extends('front.layouts.app')
@section('content')
    <section id="cart" class="bg-white">
        <div class="container">
            <div class="row py-5">
                <div class="row col-12 align-items-center text-center mx-auto mb-5 pb-lg-5">
                    <h1 class="col-auto col-lg-12 font-weight-bold px-0  mr-1 mr-lg-0 mb-0 mb-lg-2">{{app()->getLocale() === 'ro' ? 'Cos de cumparaturi' : 'Корзина покупок'}}</h1>
                    <div class="col-auto col-lg-12 breadcrumb justify-content-center mb-0 ml-auto">
                        <ul class="breadcrumb_list mb-0">
                            <li class="breadcrumb_list_item"><a href="/" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Pagina principala' : 'Главная'}}</a></li>
                            <li class="breadcrumb_list_item active mr-0"><a href="/cart" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Cos de cumparaturi' : 'Корзина покупок'}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <cart :lang="{{json_encode(app()->getLocale())}}"
                  :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}"/>
        </div>
    </section>
@endsection
