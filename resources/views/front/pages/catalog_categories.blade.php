@extends('front.layouts.app')
@section('content')
    <div id="catalog_categories_page" class="mb-4">
        <div class="container">
            <div class="row col-12 align-items-center px-0 mx-auto mb-4 mb-lg-5 pb-lg-5 mt-4">
                <h1 class="col-auto mobile_size-page_name col-lg-12 font-weight-bold px-0  mr-1 mr-lg-0 mb-0 mb-lg-2">{{app()->getLocale() === 'ro' ? 'Categorii' : 'Категории'}}</h1>
                <div class="col-auto col-lg-auto breadcrumb mb-0 ml-lg-0 mr-lg-auto">
                    <ul class="breadcrumb_list mb-0">
                        <li class="breadcrumb_list_item"><a href="/" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Pagina principală' : 'Главная'}}</a></li>
                        <li class="breadcrumb_list_item active mr-0">
                            <a href="/catalog" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Categorii' : 'Категории'}}</a></li>
                    </ul>
                </div>
            </div>

            <div class="row align-items-center">
                @foreach($categories as $category)
                <div class="col-12 col-sm-6 col-xsm-6 col-lg-4 mb-4">
                    <a href="{{url('/catalog/'.$category->slug)}}">
                        <div class="img text-center mb-4">
                            <img src="{{$category->image}}" alt="">
                        </div>
                        <div class="text-center name text-black font-weight-bold">
                            <span>{{$category->lang->name}}</span>
                        </div>
                    </a>
                </div>
                @endforeach
                    <div class="col-12 col-sm-6 col-xsm-6 col-lg-4 mb-4">
                        <a href="{{url('/catalog/shoes')}}">
                            <div class="img text-center mb-4">
                                <img src="/images/incaltaminte.png" alt="">
                            </div>
                            <div class="text-center name text-black font-weight-bold">
                                <span>{{app()->getLocale() === 'ro' ? 'Încălțăminte' : 'Обувь'}}</span>
                            </div>
                        </a>
                    </div>
            </div>
        </div>
    </div>
@endsection
