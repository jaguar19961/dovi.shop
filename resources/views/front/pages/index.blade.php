@extends('front.layouts.app')
@section('content')
    {{--    <index :lang="{{json_encode(app()->getLocale())}}"--}}
    {{--           :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}"--}}
    {{--           :categories="{{json_encode($categories)}}"--}}
    {{--           :images="{{json_encode($images)}}"--}}
    {{--           :populars="{{json_encode($populars)}}"></index>--}}
    <div class="bg-white pt-lg-5">
        <home-slider :images="{{json_encode($images)}}"></home-slider>
        <div class="group_sections">
            <div class="section_tranding">
                <div class="d-flex position-relative justify-content-center pb-lg-5">
                    <div class="section-title-vertical">
{{--                        <span class="text-white">DISCOV</span>ER NOW--}}
                    </div>
                </div>
                <div class="container mt-lg-5 pt-4">
                    <div class="row">
                        <div class="col-12 col-lg-5 d-flex align-self-center">
                            <img src="/images/logo/logo_white.svg" height="78" class="mx-auto mx-lg-3"/>
                        </div>
                        <div class="col-lg-1 align-self-center">
                            <div class="v-divider d-none d-lg-block my-3 bg-white"></div>
                            <div class="h-divider d-block d-lg-none my-3 mx-auto bg-white"></div>
                        </div>
                        <div class="col-lg-6">
                            <p class="text-white font-weight-light">
	                            {{vars(957066)}}
                            </p>
                            <!--                            <p class="normal text-lg-left text-center">-->
                            <!--                                <span class="text-white font-weight-semibold">JONT HENRRY</span>-->
                            <!--                                <span class="text-white">- CEO NESOS</span>-->
                            <!--                            </p>-->
                        </div>
                    </div>
                </div>

            </div>
            <div class="w-100 position-relative position-lg-relative bottom-sliders">
                <div class="container">
                    <div class="section_tranding_offer row justify-content-between">
                        <div class="slider_tranding col-12 col-lg-auto">
                            <el-carousel class="slider_offer" trigger="click">
                                @foreach($populars as $item)
                                    <a href="/product/{{$item->slug}}">
                                    <el-carousel-item>
                                        <div class="img">
                                            <img class="slider_image_trand" src="{{$item->image ? $item->image->url : ''}}" alt="">
                                        </div>

                                        @if($item->lang)
                                            <h4 class="title"> {{$item->lang->name}}</h4>
                                    @endif
                                    <!--                                        <span class="desc" v-if="item.lang" v-html="item.lang.description"></span>-->
                                    </el-carousel-item>
                                    </a>
                                @endforeach
                            </el-carousel>
                        </div>
                        <div class="offer_image col-12 col-lg-6 justify-self-end">
                            <div class="title">
                                <span
                                    class="text-primary text-lg-white mx-1">{{vars(531096)}} </span>
                                <span class="text-white mx-1"> {{vars(446798)}}</span>
                            </div>

                            <div class="content" onclick="window.location.href = '/catalog/imbracaminte-1'">
                                <div class="img">
                                    <img src="/images/bg_slider.png" alt="">
                                    <div class="detail">
                                        <svg xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="164" height="164"
                                             viewBox="0 0 164 164">
                                            <defs>
                                                <filter id="Ellipse_bg" x="0" y="0" width="164" height="164"
                                                        filterUnits="userSpaceOnUse">
                                                    <feOffset input="SourceAlpha"/>
                                                    <feGaussianBlur stdDeviation="19" result="blur"/>
                                                    <feFlood flood-color="#191817" flood-opacity="0.31"/>
                                                    <feComposite operator="in" in2="blur"/>
                                                    <feComposite in="SourceGraphic"/>
                                                </filter>
                                            </defs>
                                            <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#Ellipse_bg)">
                                                <circle id="Ellipse_bg-2" data-name="Ellipse bg" cx="25" cy="25" r="25"
                                                        transform="translate(57 57)" fill="#191817"/>
                                            </g>
                                            <path id="view" d="M1259,1963v-9h-9v-1h9v-10h1v10h10v1h-10v9Z"
                                                  transform="translate(-1178 -1871)" fill="#fff"/>
                                        </svg>
                                    </div>

                                </div>
                                <div class="desc">
                                    <span
                                        class="desc_title">{{vars(644490)}}</span>
                                    <span
                                        class="desc_desc">{{vars(458612)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="mt-5">
            <div class="container">
                <popular-slider :lang="{{json_encode(app()->getLocale())}}"
                                :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}"
                                :categories="{{json_encode($categories)}}"></popular-slider>
            </div>
        </section>


        <!-- <div>
             <div class="container">
                <div class="row position-realitve">
                    <div class="col-12 text-center text-lg-left col-lg-auto produc-desc-slide-deal mx-auto mx-lg-0 justify-content-center">
                        <h1 class="text-primary font-weight-bold">Deal off the day</h1>
                        <p class="text-uppercase lh-200 py-0 my-0 normal mt-2">NESOS WHITE T-SHIRT SUMMER NEW</p>
                        <p class="text-uppercase lh-200 py-0 my-0 normal text-gray-800">350 MDL</p>
                        <div class="mt-5">
                            <a href="" class="text-uppercase ls-2 lh-200 font-weight-bold text-primary normal text-decoration-none">ADD TO CART</a>
                        </div>
                    </div>
                    <div class="col-auto mx-auto mx-lg-0 bg-circle-slider-deal">
                        <img src="/images/cross-1x1.png" class="image-product-slide-deal">

                    </div>
                    <div class="col-auto count-slide-deal d-flex align-self-end justif-self-end px-0">
                        <count-down date="2021-04-06 08:15:00"></count-down>
                    </div>
                </div>
        </div>
        </div>  -->
        <!--        <div class="deal_of_day">-->

        <!--                <deal-slider></deal-slider>-->
        <!--        </div>-->

        <div class="pb-5 pt-5">
            <div class="container">
                <div class="row pt-5 mb-5 text-center text-lg-left">
                    <div class="col-lg-3 mb-3 mb-lg-0">
                        <h3 class="font-weight-600 h5">{{vars(627633)}}</h3>
                        <p class="text-secondary letter-spacing-3">{{vars(475095)}}</p>
                    </div>
                    <div class="col-lg-3 mb-3 mb-lg-0">
                        <h3 class="font-weight-600 h5">{{vars(310870)}}</h3>
                        <p class="text-secondary letter-spacing-3">{{vars(705165)}}</p>
                    </div>
                    <div class="col-lg-3 mb-3 mb-lg-0">
                        <h3 class="font-weight-600 h5">{{vars(800433)}}</h3>
                        <p class="text-secondary letter-spacing-3">{{vars(541965)}}</p>
                    </div>
                    <div class="col-lg-3 mb-3 mb-lg-0">
                        <h3 class="font-weight-600 h5">{{vars(405519)}}</h3>
                        <p class="text-secondary letter-spacing-3">{{vars(416724)}} </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
