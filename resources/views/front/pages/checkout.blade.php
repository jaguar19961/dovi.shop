@extends('front.layouts.app')
@section('content')
    <section id="checkout" class="bg-white">
        <div class="container">
            <checkout :lang="{{json_encode(app()->getLocale())}}"
                      :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}"></checkout>
        </div>
    </section>
@endsection
