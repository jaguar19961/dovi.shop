@extends('front.layouts.app')
@section('content')
    <section id="login_register" class="bg-white">
        <div class="container pb-5">
            <div class="row pt-5 pb-0 py-lg-5">
                <div class="text-center w-100 text-center w-100 mb-0 mb-lg-5 pb-5">
                    <h1 class="font-weight-bold">{{app()->getLocale() === 'ro' ? 'Inregistrare' : 'Cтраница авторизации'}}</h1>
                    <div class="breadcrumb justify-content-center">
                        <ul class="breadcrumb_list">
                            <li class="breadcrumb_list_item">
                                <a href="/" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Pagina principala' : 'Главная'}}</a>
                            </li>
                            <li class="breadcrumb_list_item active mr-0">
                                <a href="/user_register" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Inregistrare' : 'Cтраница авторизации'}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-12 offset-lg-1 pt-5 pt-lg-0">
                    <h3 class="font-weight-bold">{{app()->getLocale() === 'ro' ? 'Inregistreaza un cont nou' : 'Зарегистрируйте новую учетную запись'}} </h3>
                    <p class="text-gray-700">{{app()->getLocale() === 'ro' ? 'Inregistreaza-te si ai acces la istoricul de cumparaturi' : 'Зарегистрируйтесь и получите доступ к своей истории покупок'}}</p>

                    <!-- REGISTER FORM -->
                    <form class="container" method="POST" action="/register-front">
                        @csrf
                        <div class="row py-3">
                            <div class="col-12 col-lg-6 pb-3 pb-lg-0 px-0 pr-lg-2">
                                <input type="text"
                                       class="form-control form-control-extra-lighter rounded-pill pl-4 @error('first_name') is-invalid @enderror"
                                       placeholder="Your Name"
                                       name="first_name"
                                       value="{{ old('first_name') }}" required autocomplete="first_name" autofocus/>
                                @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12 pt-3 pt-lg-0 col-lg-6 px-0 pl-lg-2">
                                <input type="text"
                                       class="form-control form-control-extra-lighter rounded-pill pl-4 @error('last_name') is-invalid @enderror"
                                       placeholder="User Name"
                                       name="last_name"
                                       value="{{ old('last_name') }}" required autocomplete="last_name"/>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-12 col-lg-6 pb-3 pb-lg-0 px-0 pr-lg-2">
                                <input type="text"
                                       class="form-control form-control-extra-lighter rounded-pill pl-4 @error('phone') is-invalid @enderror"
                                       placeholder="Phone"
                                       name="phone"
                                       value="{{ old('phone') }}" required autocomplete="phone"/>
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12 pt-3 pt-lg-0 col-lg-6 px-0 pl-lg-2">
                                <input type="text"
                                       class="form-control form-control-extra-lighter rounded-pill pl-4 @error('email') is-invalid @enderror"
                                       placeholder="Email"
                                       name="email"
                                       value="{{ old('email') }}" required autocomplete="email"/>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row py-3 ">
                            <div class="col-12 col-lg-6 pb-3 pb-lg-0 px-0 pr-lg-2">
                                <input type="password"
                                       class="form-control form-control-extra-lighter rounded-pill pl-4 @error('password') is-invalid @enderror"
                                       placeholder="Password"
                                       name="password"
                                       required autocomplete="new-password"/>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-12 pt-3 pt-lg-0 col-lg-6 px-0 pl-lg-2">
                                <input type="password"
                                       class="form-control form-control-extra-lighter rounded-pill pl-4 @error('password_confirmation') is-invalid @enderror"
                                       placeholder="Confirm Password"
                                       name="password_confirmation"
                                       required autocomplete="new-password"/>
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="custom-control custom-radio custom-control-inline text-gray-700 col-12 mr-0">
                                <input class="custom-control-input @error('terms') is-invalid @enderror"
                                       type="checkbox"
                                       name="terms"
                                       id="exampleRadios2" value="true">
                                <label class="custom-control-label font-weight-regular" for="exampleRadios2">
                                    {{app()->getLocale() === 'ro' ? 'Accept termenii, inclusiv Politica de confidențialitate' : 'Я принимаю условия, в том числе Политику конфиденциальности'}}
                                </label>
                            </div>
                        </div>
                        <div class="row py-3 justify-content-center justify-content-lg-start">
                            <button type="submit"
                                    class="btn btn-lg rounded-pill text-white font-weight-bold btn-primary col-5">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </form>
                    <!-- SOCIAL REGISTER -->

                </div>
            </div>
        </div>
    </section>
@endsection
