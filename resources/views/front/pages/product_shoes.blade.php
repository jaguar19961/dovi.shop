@extends('front.layouts.app')
@section('content')
    <product-shoes :model="{{json_encode($model)}}"
                   :name="{{json_encode($name)}}"
                   :lang="{{json_encode(app()->getLocale())}}"
                   :auth="{{json_encode(auth()->check())}}"></product-shoes>
@endsection
