@extends('front.layouts.app')
@section('content')
    <section id="login_register" class="bg-white">
        <div class="container pb-5">
            <div class="row pt-5 pb-0 py-lg-5">
                <div class="text-center w-100 text-center w-100 mb-0 mb-lg-5 pb-5">
                    <h1 class="font-weight-bold">{{app()->getLocale() === 'ro' ? 'Logare' : 'Cтраница авторизации'}}</h1>
                    <div class="breadcrumb justify-content-center">
                        <ul class="breadcrumb_list">
                            <li class="breadcrumb_list_item">
                                <a href="/" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Pagina principala' : 'Главная'}}</a></li>
                            <li class="breadcrumb_list_item active mr-0">
                                <a href="/login_register" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Logare' : 'Cтраница авторизации'}}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-5">
                    <h3 class="font-weight-bold">{{app()->getLocale() === 'ro' ? 'Logare in contul personal' : 'Войдите в личный кабинет'}}</h3>
                    <p class="text-gray-700">{{app()->getLocale() === 'ro' ? 'Logare in cabinetul personal sau inregistrativa' : 'Войдите в свою учетную запись или зарегистрируйтесь'}} <a href="/user_register">{{app()->getLocale() === 'ro' ? 'Inregistrare' : 'Регистрация'}}</a></p>

                    <!-- LOGIN FORM -->
                    <form class="container" action="/login-front" method="POST">
                        @csrf
                        <div class="row py-3">
                            <input type="text"
                                   class="form-control form-control-extra-lighter rounded-pill pl-4 @error('email') is-invalid @enderror"
                                   placeholder="Email"
                                   name="email"
                                   value="{{ old('email') }}" required autocomplete="email" autofocus/>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="row py-3">
                            <input type="password"
                                   class="form-control form-control-extra-lighter rounded-pill pl-4  @error('password') is-invalid @enderror"
                                   placeholder="Password"
                                   name="password"
                                   required autocomplete="current-password"
                            />
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="row py-3 px-2">
                            <div
                                class="custom-control custom-radio custom-control-inline text-gray-700 col-12 col-lg-5 mr-0">
                                <input class="custom-control-input" type="checkbox"
                                       name="remember" id="remember" value="option1"
                                    {{ old('remember') ? 'checked' : '' }}>
                                <label class="custom-control-label font-weight-regular" for="remember">
                                    <span class="align-middle">{{ __('Remember Me') }}</span>
                                </label>
                            </div>
                            @if (Route::has('password.request'))
                                <div class="col-12 col-lg-7 my-2 my-lg-0">
                                    <a href="{{ route('password.request') }}" class="text-primary align-middle">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                </div>
                            @endif
                        </div>
                        <div class="row py-3 justify-content-center justify-content-lg-start">
                            <button type="submit"
                                    class="btn btn-lg rounded-pill text-white font-weight-bold btn-primary col-5"> {{ __('Login') }}
                            </button>
                        </div>
                        {{--                        <div class="row pt-5 pb-3">--}}
                        {{--                            <div class="col-12 col-lg-4 text-center text-lg-left"><span class="text-secondary font-weight-semibold">OR LOGIN WITH</span></div>--}}
                        {{--                            <div class="col-12 col-lg-8 d-flex justify-content-center justify-content-lg-start my-3 my-lg-0">--}}
                        {{--                                <a href="#" class="d-inline-block text-gray-700 hover-primary mx-3">--}}
                        {{--                                    <i class="fab fa-facebook-f fa-lg"></i>--}}
                        {{--                                </a>--}}
                        {{--                                <a href="#" class="d-inline-block text-gray-700 hover-primary mx-3">--}}
                        {{--                                    <i class="fab fa-twitter fa-lg"></i>--}}
                        {{--                                </a>--}}
                        {{--                                <a href="#" class="d-inline-block text-gray-700 hover-primary mx-3">--}}
                        {{--                                    <i class="fab fa-google fa-lg"></i>--}}
                        {{--                                </a>--}}
                        {{--                                <a href="#" class="d-inline-block text-gray-700 hover-primary mx-3">--}}
                        {{--                                    <i class="fab fa-instagram fa-lg"></i>--}}
                        {{--                                </a>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </form>
                    <!-- SOCIAL LOGIN -->

                </div>
                <div class="col-12 col-lg-7"></div>
            </div>
        </div>
    </section>
@endsection
