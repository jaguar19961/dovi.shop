@extends('front.layouts.app')
@section('content')
    <section id="thank_you_page" class="bg-white overflow-hidden">
        <div class="container">
            <div class="row no-gutters text-center my-5">
                <h1 class="text-primary font-weight-bold w-100 my-2">{{$model->lang->name}}</h1>
            </div>
            <div class="row no-gutters justify-content-center pb-2 mb-5">
                <div class="col-lg-11 px-0">
                    <div class="lh-200 text-center my-5">
                        {!! $model->lang->description !!}
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
