@extends('front.layouts.app')
@section('content')

    <section id="section section-lg about_us ">
        <div class="container">
            <div class="row py-5">
                <div class="row col-12 align-items-center text-center mx-auto mb-5 pb-lg-5">
                    <h1 class="col-auto col-lg-12 font-weight-bold px-0  mr-1 mr-lg-0 mb-0 mb-lg-2 d-xsm-none">{{app()->getLocale() === 'ro' ? 'Despre noi' : 'О нас'}}</h1>
                    <div class="col-auto col-lg-12 breadcrumb justify-content-center text-left mb-0 mr-auto">
                        <ul class="breadcrumb_list mb-0">
                            <li class="breadcrumb_list_item"><a href="/" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Pagina principala' : 'Главная'}}</a></li>
                            <li class="breadcrumb_list_item active mr-0"><a href="/about-us" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Despre noi' : 'О нас'}}</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-5">
                    <h1 class="font-weight-bold">{{vars('875741')}}</h1>
                </div>
                <div class="col-lg-1">
                    <div class="v-divider d-none d-lg-block my-3"></div>
                    <div class="h-divider d-lg-none mb-4 mt-3"></div>
                </div>
                <div class="col-lg-6">
                    <p class="text-secondary pb-4">
                        {{vars('366661')}}
                    </p>
{{--                    <div class="mb-3">--}}
{{--                        <img src="/images/signature-1.svg"/>--}}
{{--                    </div>--}}
{{--                    <p><span class="text-black">{{vars('255269')}}</span><span class="text-secondary"> - {{vars('299226')}}</span></p>--}}
                </div>
            </div>
            <div class="w-100 my-3 my-lg-5 bg-rect" style="background: url('/images/bg_contacte.jpg') center center / cover;"></div>
            <div class="row pt-5 mb-5 text-center text-lg-left">
                <div class="col-lg-3 mb-3 mb-lg-0">
                    <h3 class="font-weight-600 h5">{{vars('300578')}}</h3>
                    <p class="text-secondary letter-spacing-3">{{vars('977615')}}</p>
                </div>
                <div class="col-lg-3 mb-3 mb-lg-0">
                    <h3 class="font-weight-600 h5">{{vars('797204')}}</h3>
                    <p class="text-secondary letter-spacing-3">{{vars('217069')}}</p>
                </div>
                <div class="col-lg-3 mb-3 mb-lg-0">
                    <h3 class="font-weight-600 h5">{{vars('966304')}}</h3>
                    <p class="text-secondary letter-spacing-3">{{vars('873962')}}</p>
                </div>
                <div class="col-lg-3 mb-3 mb-lg-0">
                    <h3 class="font-weight-600 h5">{{vars('625410')}}</h3>
                    <p class="text-secondary letter-spacing-3">{{vars('959626')}}</p>
                </div>
            </div>
        </div>
    </section>
@endsection
