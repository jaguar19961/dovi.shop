<div class="col-12 col-lg-3  d-lg-block d-none">
    <div class="card bg-lighter border-lighter p-4">
        <ul class="list-unstyled">
            <li class="d-flex lh-300 border-bottom">
                <a href="/account/my_account" class="text-gray-400">{{app()->getLocale() === 'ro' ? 'Contul meu' : 'Мой аккаунт'}}</a>
            </li>
            <li class="d-flex lh-300 border-bottom">
                <a href="/account/orders" class="text-gray-400">{{app()->getLocale() === 'ro' ? 'Comenzi' : 'Заказы'}}</a>
            </li>
            <li class="d-flex lh-300">
                <a href="/account/whishlist" class="text-gray-400">{{app()->getLocale() === 'ro' ? 'Favorite' : 'Избранное'}}</a>
            </li>
        </ul>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
           role="button" class="btn btn-lg btn-black rounded-pill text-uppercase font-weight-bold">{{ __('Logout') }}</a>
    </div>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST"
      style="display: none;">
    @csrf
</form>
