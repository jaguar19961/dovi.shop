@extends('front.layouts.app')
@section('content')
    <section id="orders" class="bg-white">
        <div class="container pb-5">
            <div class="row pt-5 pb-0 py-lg-5">
                <div class="text-center w-100 text-center w-100 mb-0 mb-lg-5 pb-4 pb-lg-5">
                    <h1 class="font-weight-bold">{{app()->getLocale() === 'ro' ? 'Comenzi' : 'Заказы'}}</h1>
                    <div class="breadcrumb justify-content-center mt-3 mt-lg-0">
                        <ul class="breadcrumb_list">
                            <li class="breadcrumb_list_item"><a href="/" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Pagina principala' : 'Главная'}}</a></li>
                            <li class="breadcrumb_list_item active mr-0"><a href="/account/orders" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Comenzi' : 'Заказы'}}</a></li>
                        </ul>
                    </div>
                </div>
                @include('front.pages.account.menu')
                <div class="col-12 d-lg-none">
                    <a class="w-100 d-block p-3 bg-lighter my-2 font-weight-semibold text-decoration-none" href="/account/my_account">
                        <span><i class="el-icon-caret-right text-gray-400 mr-2"></i></span>
                        <span class="text-gray-400">{{app()->getLocale() === 'ro' ? 'Cont' : 'Аккаунт'}}</span>
                    </a>
                    <a class="w-100 d-block p-3 bg-lighter my-2 font-weight-semibold text-decoration-none" href="/account/orders">
                       <span><i class="el-icon-caret-bottom text-gray-400 mr-2"></i></span>
                       <span class="text-black">{{app()->getLocale() === 'ro' ? 'Comenzi' : 'Заказы'}}</span>
                    </a>
                </div>
                <orders :lang="{{json_encode(app()->getLocale())}}" :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}"></orders>
                <div class="col-12 d-lg-none">
                    <a class="w-100 d-block p-3 bg-lighter my-2 font-weight-semibold text-decoration-none" href="/account/whishlist">
                        <span><i class="el-icon-caret-right text-gray-400 mr-2 "></i></span>
                        <span class="text-gray-400">{{app()->getLocale() === 'ro' ? 'Favorite' : 'Избранное'}}</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
