@extends('front.layouts.app')
@section('content')
    <section id="my_account" class="bg-white">
        <div class="container pb-5">
            <div class="row pt-5 pb-0 py-lg-5">
                <div class="text-center w-100 text-center w-100 mb-0 mb-lg-5 pb-4 pb-lg-5">
                    <h1 class="font-weight-bold">{{app()->getLocale() === 'ro' ? 'Contul meu' : 'Мой аккаунт'}}</h1>
                    <div class="breadcrumb justify-content-center mt-3 mt-lg-0">
                        <ul class="breadcrumb_list">
                            <li class="breadcrumb_list_item"><a href="/" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Pagina principala' : 'Главная'}}</a></li>
                            <li class="breadcrumb_list_item active mr-0"><a href="/account/my_account" class="breadcrumb_list_item_link">{{app()->getLocale() === 'ro' ? 'Contul meu' : 'Мой аккаунт'}}</a></li>
                        </ul>
                    </div>
                </div>
                @include('front.pages.account.menu')
                <div class="col-12 d-lg-none">
                    <a class="w-100 d-block p-3 bg-lighter my-2 font-weight-semibold text-decoration-none" href="#">
                        <span><i class="el-icon-caret-bottom text-gray-400 mr-2"></i></span>
                        <span class="text-black">{{app()->getLocale() === 'ro' ? 'Contul meu' : 'Мой аккаунт'}}</span>
                    </a>
                </div>
                <form method="POST" action="/register-front-update" class="col-12 col-lg-9">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-lg-4 mt-2 mb-3">
                            <label class="form-label font-weight-medium text-secondary">{{app()->getLocale() === 'ro' ? 'NUME' : 'ИМЯ'}}</label>
                            <input type="text"
                                   class="form-control form-control-extra-lighter rounded-pill pl-4 @error('first_name') is-invalid @enderror"
                                   name="first_name"
                                   value="{{ auth()->user()->first_name }}" required autocomplete="first_name" autofocus/>
                            @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="col-12  col-lg-4 mt-2 mb-3">
                            <label class="form-label font-weight-medium text-secondary">{{app()->getLocale() === 'ro' ? 'NUMELE DE FAMILIE' : 'ФАМИЛИЯ'}}</label>
                            <input type="text"
                                   class="form-control form-control-extra-lighter rounded-pill pl-4 @error('last_name') is-invalid @enderror"
                                   name="last_name"
                                   value="{{ auth()->user()->last_name }}" required autocomplete="last_name"/>
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="col-12  col-lg-4 mt-2 mb-3">
                            <label class="form-label font-weight-medium text-secondary">{{app()->getLocale() === 'ro' ? 'TELEFON' : 'ТЕЛЕФОН'}}</label>
                            <input type="text"
                                   class="form-control form-control-extra-lighter rounded-pill pl-4 @error('phone') is-invalid @enderror"
                                   name="phone"
                                   value="{{ auth()->user()->phone }}" required autocomplete="phone"/>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
{{--                    <div class="row">--}}
{{--                        <div class="col-12  col-lg-12 mt-2 mb-3">--}}
{{--                            <label class="form-label font-weight-medium text-secondary">ADDRESS</label>                            </label>--}}
{{--                            <input type="text" class="form-control form-control-extra-lighter rounded-pill pl-4"/>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-12  col-lg-12 mt-2 mb-3">--}}
{{--                            <label class="form-label font-weight-medium text-secondary">CITY / TOWN</label>                            </label>--}}
{{--                            <input type="text" class="form-control form-control-extra-lighter rounded-pill pl-4"/>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="row">
                        <div class="col-12  col-lg-4 mt-2 mb-3">
                            <label class="form-label font-weight-medium text-secondary">{{app()->getLocale() === 'ro' ? 'PAROLA' : 'ПАРОЛЬ'}}</label>
                            <input type="password" class="form-control form-control-extra-lighter rounded-pill pl-4"/>
                        </div>
                        <div class="col-12  col-lg-4 mt-2 mb-3">
                            <label class="form-label font-weight-medium text-secondary">{{app()->getLocale() === 'ro' ? 'PAROLĂ NOUĂ' : 'НОВЫЙ ПАРОЛЬ'}}</label>
                            <input type="password" class="form-control form-control-extra-lighter rounded-pill pl-4"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="justify-content-center d-flex col-lg-8 mt-2 mb-3">
                            <button type="submit" class="btn btn-primary  rounded-pill px-4 text-white font-weight-bold normal">{{app()->getLocale() === 'ro' ? 'SALVEAZĂ MODIFICĂRILE' : 'СОХРАНИТЬ ИЗМЕНЕНИЯ'}}</button>
                        </div>
                    </div>
                </form>
                <div class="col-12 d-lg-none">
                    <a class="w-100 d-block p-3 bg-lighter my-2 font-weight-semibold text-decoration-none" href="/account/orders">
                       <span><i class="el-icon-caret-right text-gray-400 mr-2"></i></span>
                       <span class="text-gray-400">{{app()->getLocale() === 'ro' ? 'COMENZI' : 'ЗАКАЗЫ'}}</span>
                    </a>
                    <a class="w-100 d-block p-3 bg-lighter my-2 font-weight-semibold text-decoration-none" href="/account/whishlist">
                        <span><i class="el-icon-caret-right text-gray-400 mr-2 "></i></span>
                        <span class="text-gray-400">{{app()->getLocale() === 'ro' ? 'FAVORITE' : 'Избранное'}}</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
