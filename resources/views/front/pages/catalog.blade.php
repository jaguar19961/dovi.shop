@extends('front.layouts.app')
@section('content')
    <catalog :categories="{{json_encode($categories)}}"
             :lang="{{json_encode(app()->getLocale())}}"
             :category_id="{{json_encode($category)}}"
             :url="{{json_encode($url)}}"
             :course="{{json_encode(\Illuminate\Support\Facades\Session::get('valute'))}}"></catalog>
@endsection
