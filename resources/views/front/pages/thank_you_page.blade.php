@extends('front.layouts.app')
@section('content')
    <section id="thank_you_page" class="bg-white overflow-hidden">
        <div class="container">
            <div class="row no-gutters justify-content-center mt-5">
                <img src="/images/thankyou-illustration.svg" />
            </div>
            <div class="row no-gutters text-center my-5">
                <h1 class="text-primary font-weight-bold w-100 my-2">{{app()->getLocale() === 'ro' ? 'Va multumum' : 'Благодарю вас'}}</h1>
                <p class="h3 w-100">{{app()->getLocale() === 'ro' ? 'Comanda dvs. a fost procesată cu succes.' : 'Ваш заказ был успешно обработан.'}}</p>
            </div>
            <div class="row no-gutters justify-content-center">
                <div class="card border-light bg-lighter col-lg-5">
                    <p class="text-center h3 font-weight-bold py-5 m-0"><span class="">{{app()->getLocale() === 'ro' ? 'Numarul comenzii' : 'Номер заказа:'}}</span> <span class="text-primary">#{{$id}}</span></p>
                </div>
            </div>
            <div class="row no-gutters justify-content-center pb-2 mb-5">
                <div class="col-lg-11 px-0">
                    <p class="lh-200 text-center my-5">
                        {{app()->getLocale() === 'ro' ? 'Operatorul nostru vă va contacta în scurt timp pentru a vă confirma comanda.' : 'Вскоре с вами свяжется наш оператор для подтверждения вашего заказа.'}} <br/>
                        <span class="d-lg-none"><br/></span>
                        {{app()->getLocale() === 'ro' ? 'Un e-mail de confirmare a fost trimis la adresa de e-mail specificată. Dacă nu ați primit confirmarea în decurs de o oră, vă rugăm ' : 'На указанный адрес электронной почты было отправлено письмо с подтверждением. Если вы не получили подтверждения в течение часа, пожалуйста,'}}<a href="/contact" class="text-underline text-black">{{app()->getLocale() === 'ro' ? 'Contactează-ne' : 'Свяжитесь с нами'}}</a>.
                    </p>
                </div>
                <a class="btn btn-lg rounded-pill text-white  font-weight-bold btn-primary" href="/catalog">{{app()->getLocale() === 'ro' ? 'CONTINUA CUMPARATURILE' : 'ПРОДОЛЖИТЬ ПОКУПКИ'}}</a>
            </div>
        </div>

    </section>
@endsection
