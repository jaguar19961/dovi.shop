@extends('front.layouts.app')
@section('content')
    <product :model="{{json_encode($model)}}" :lang="{{json_encode(app()->getLocale())}}" :auth="{{json_encode(auth()->check())}}"></product>
@endsection
