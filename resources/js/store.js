import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import Vue from 'vue';
import {Notification} from 'element-ui'
import createPersistedState from 'vuex-persistedstate';

Vue.prototype.$notify = Notification;

Vue.use(Vuex);
const vuexLocalStorage = new VuexPersist({
    key: 'vuex',
    // The key to store the state on in the storage provider.
    storage: window.localStorage, // or window.sessionStorage or localForage
    // Function that passes the state and returns the state with only the objects you want to store.
    // reducer: state => state,
    // Function that passes a mutation and lets you decide if it should update the state in localStorage.
    // filter: mutation => (true)
})

export default new Vuex.Store({
    plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
    state: {
        cart: [],
        product: [],
        compares: [],
    },

    getters: {
        availableProducts(state, getters) {

        },

        cartProducts(state) {
            return state.cart.map(cartItem => {
                const product = state.product.find(product => product.id === cartItem.id)
                return {
                    id: cartItem.id,
                    quantity: cartItem.quantity,
                    color: cartItem.color,
                    specification: cartItem.specification,
                    type: cartItem.type,
                }
            })
        },

        compareProducts(state) {
            return state.compares.map(cartItem => {
                const product = state.compares.find(product => product.id === cartItem.id)
                return {
                    id: cartItem.id,
                    image: cartItem.image,
                    name_ro: cartItem.name_ro,
                    name_ru: cartItem.name_ru,
                    slug: cartItem.slug,
                    price: cartItem.price,
                    quantity: cartItem.quantity,
                }
            })
        },

        cartTotal(state, getters) {
            return getters.cartProducts.reduce((total, product) => total + product.price * product.quantity, 0)
        },

        countCart(statem, getters) {
            return getters.cartProducts.reduce((total, product) => total + product.quantity, 0)
        },

        checkIfInCompare(state, getter) {
            return 1;
        },
    },

    actions: {
        addProductToCart(context, product) {
            const cartItem = context.state.cart.find(item => item.id === product.item.id &&
                item.specification.toString() === product.specification.toString() &&
                item.color === product.color &&
                item.type === product.type)
            let cart = {
                cartItem: cartItem,
                quantity: product.quantity,
                specification: product.specification,
                color: product.color,
                type: product.type,
            }
            if (!cartItem) {
                Notification.success({
                    title: 'Succes',
                    message: 'Produsul a fost adaugat in cos cu success',
                    type: 'success',
                    position: 'bottom-right'
                });
                context.commit('pushProductToCart', product)
            } else {
                let item = {
                    data: cartItem,
                    quantity: product.quantity,
                    type: product.type
                }
                Notification.success({
                    title: 'Succes',
                    message: 'Produsul a fost incrementat cu success',
                    type: 'success',
                    position: 'bottom-right'
                });
                context.commit('udpateCartItem', item)
            }

        },

        updateItemQuantity(context, product) {
            // console.log(product);
            let cartItem = [];
            if (product.type == 1) {
                console.log('test1')
                cartItem = context.state.cart.find(item => item.id === product.id &&
                    item.specification.toString() === product.specification.toString() &&
                    item.color === product.color &&
                    item.type === product.type);
            }
            if (product.type == 2) {
                console.log('test')
                cartItem = context.state.cart.find(item => item.id === product.id &&
                    item.specification === product.specification &&
                    item.type === product.type);
            }

            if (cartItem.quantity >= 1) {
                if (cartItem) {
                    let item = {
                        data: cartItem,
                        specification: product.specification,
                        color: product.color,
                        quantity: product.quantity,
                        type: product.type
                    }
                    context.commit('udpateCartItem', item)
                }
            } else {
                context.commit('deleteItem', cartItem);
            }
        },

        incrementItemQuantity(context, product) {
            if (product.quantity > 0) {
                const cartItem = context.state.cart.find(item => item.id === product.id && _.isEqual(item.specification, product.specification))
                if (!cartItem) {
                    context.commit('pushProductToCart', product)
                } else {
                    context.commit('incrementItemQuantity', cartItem)
                }
            }
        },

        decrementProductInventory(context, product) {
            const cartItem = context.state.cart.find(item => item.id === product.id)
            if (cartItem.quantity > 1) {
                if (cartItem) {
                    context.commit('decrementProductInventory', cartItem)
                }
            } else {
                context.commit('deleteItem', cartItem);
            }
        },

        deleteFromStore(context, product) {
            const cartItem = context.state.cart.find(item => item.id === product.id &&
                item.specification.toString() === product.specification.toString() &&
                item.type === product.type);
            context.commit('deleteItem', cartItem);
        },

        resetCartStore(context) {
            context.commit('resetCart');
        },

        resetCompareStore(context) {
            context.commit('resetCompare');
        },

        addProductCompare(context, product) {
            let selected = context.state.compares.find(item => item.id === product.id) ? true : false
            let firstSelectedProductCat = true;
            if (context.state.compares.length > 0) {
                if (context.state.compares[0].category_id !== product.category_id || context.state.compares[0].sub_category_id !== product.sub_category_id) {
                    firstSelectedProductCat = true
                } else {
                    firstSelectedProductCat = false
                }
            } else {
                firstSelectedProductCat = false
            }

            if (context.state.compares.length <= 3 && !selected && !firstSelectedProductCat) {
                context.commit('addProductCompare', product);
                Notification.success({
                    title: 'Success',
                    message: 'Acest produs a fost adaugat cu succes la comparatie',
                    type: 'success',
                    position: 'bottom-right'
                });
            } else {
                if (firstSelectedProductCat) {
                    Notification.warning({
                        title: 'Atentie',
                        message: 'Acest produs nu face parte din categoria la produsele adaugate anterior',
                        type: 'warning',
                        position: 'bottom-right'
                    });
                } else {
                    Notification.warning({
                        title: 'Atentie',
                        message: 'Acest produs a fost deja adaugat la comparatie',
                        type: 'warning',
                        position: 'bottom-right'
                    });
                }

            }

        },

        deleteProductItemCompare(context, product) {
            context.commit('deleteProductItemCompare', product);
        }
    },

    mutations: {

        // --------------------
        addProductCompare(state, product) {
            state.compares.push({
                id: product.id,
                image: product.gallery_one ? product.gallery_one.path : '',
                name_ro: product.name_ro,
                name_ru: product.name_ru,
                category_id: product.category_id,
                sub_category_id: product.sub_category_id,
                slug: product.slug,
            })
        },

        deleteProductItemCompare(state, product) {
            let i = state.compares.map(item => item.id).indexOf(product.id)
            state.compares.splice(i, 1);
        },

        pushProductToCart(state, product) {
            state.cart.push({
                id: product.item.id,
                quantity: product.quantity,
                specification: product.specification,
                color: product.color,
                type: product.type,
            })
        },

        incrementItemQuantity(state, cartItem) {
            cartItem.cartItem.quantity += cartItem.quantity;
        },

        udpateCartItem(state, cartItem) {
            cartItem.data.quantity = cartItem.quantity;
        },

        decrementProductInventory(state, cartItem) {
            cartItem.quantity--
        },

        deleteItem(state, product) {
            let item = state.cart.find(item => item.id === product.id &&
                item.specification.toString() === product.specification.toString() &&
                item.color === product.color &&
                item.type === product.type);
            let a = state.cart.indexOf(item)
            state.cart.splice(a, 1);
        },

        delItm(state, index) {
            state.cart.splice(index, 1);
        },

        resetCart(state) {
            state.cart = [];
        },

        resetCompare(state) {
            state.compares = [];
        }
    }

});
