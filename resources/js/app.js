/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import {VueEditor} from "vue2-editor";

import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
// import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI, {locale});
Vue.component('pagination', require('laravel-vue-pagination'));

import store from './store';

// import VueCountdownTimer from 'vuejs-countdown-timer';
// Vue.use(VueCountdownTimer);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// pages
Vue.component('index', require('./components/pages/Index.vue').default);
Vue.component('catalog', require('./components/pages/Catalog.vue').default);
Vue.component('product', require('./components/pages/Product.vue').default);
Vue.component('product-shoes', require('./components/pages/ProductShoes.vue').default);
Vue.component('cart', require('./components/pages/Cart.vue').default);
Vue.component('checkout', require('./components/pages/Checkout.vue').default);
Vue.component('my-account', require('./components/pages/account/MyAccount.vue').default);
Vue.component('orders', require('./components/pages/account/Orders.vue').default);
Vue.component('whishlist', require('./components/pages/account/Whishlist.vue').default);

//ui
Vue.component('popular-slider', require('./components/ui/PopularSlider.vue').default);
Vue.component('deal-slider', require('./components/ui/DealSlider.vue').default);
Vue.component('deal-slider-item', require('./components/ui/DealSliderItem.vue').default);
Vue.component('product-item', require('./components/ui/ProductItem.vue').default);
Vue.component('product-item-shoes', require('./components/ui/ProductItemShoes.vue').default);
Vue.component('product-item-line', require('./components/ui/ProductItemInLine.vue').default);
Vue.component('count-down', require('./components/ui/CountDown.vue').default);
Vue.component('count-cart-footer', require('./components/ui/CountCartFooter.vue').default);
Vue.component('count-cart', require('./components/ui/CountCart.vue').default);
Vue.component('cart-item', require('./components/ui/cart/Item.vue').default);
Vue.component('search-bar', require('./components/ui/SearchBar.vue').default);
Vue.component('side-bar', require('./components/ui/SideBar.vue').default);
Vue.component('home-slider', require('./components/ui/HomeSlider.vue').default);

//vendor InputNumber
Vue.component('bootstrap-el-input-number', require('./components/vendor/InputNumber.vue').default);

//ADMIN
Vue.component('admin-products', require('./components/admin/Products.vue').default);
Vue.component('admin-product-spec-item', require('./components/admin/ProductSpecItem.vue').default);
Vue.component('admin-edit-products', require('./components/admin/EditProduct.vue').default);
Vue.component('admin-assign-attribute', require('./components/admin/CreateAttributes.vue').default);
Vue.component('admin-create-credit', require('./components/admin/CreateCredits.vue').default);

// new version
Vue.component('admin-create-new-product', require('./components/admin/CreateNewProduct.vue').default);
Vue.component('admin-create-specification', require('./components/admin/products/ProductSpecifications.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import VueCarousel from 'vue-carousel';

Vue.use(VueCarousel);

const app = new Vue({
    store,
    el: '#app',
    components: {
        VueEditor
    },
    data() {
        return {
            search_bar_mobile: false,
            search_bar: false,
            drawer: false,
        }
    },

    watch: {
        'search_bar_mobile': {
            handler(){
                window.scrollTo({top: 0, behavior: 'smooth'});
            }
        }
    },

    methods: {
        goToProd(item) {
            window.location.href = '/product/' + item.slug;
        }
    }

});
