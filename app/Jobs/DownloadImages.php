<?php

namespace App\Jobs;

use App\Models\Shoes;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\Async\Pool;
use function PHPUnit\Framework\isNull;

class DownloadImages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $data = array();

    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $collections = Shoes::get();
        foreach ($collections as $item) {
            $it = Shoes::find($item->id);
            if (!is_null($it)) {
                if (count($it->size) > 0 && is_null($it->image)) {
                    $this->explodeImage($item->remote_images, $item->id, $item->image);
                }
            }
        }
    }

    public function explodeImage($imageArray, $shoesID, $localImage)
    {
        if ($imageArray != null && empty($localImage)) {
            collect($imageArray)->each(function ($image) use ($shoesID) {
                dispatch(new DownloadOneImageJob($image, $shoesID));
            });
        }
    }
}
