<?php

namespace App\Jobs;

use App\Models\Shoes;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

/**
 * Class DownloadOneImageJob
 * @package App\Jobs
 */
class DownloadOneImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    private $imageUrl;
    /**
     * @var int
     */
    private $shoesID;

    /**
     * Create a new job instance.
     *
     * @param string $imageUrl
     * @param int $shoesID
     */
    public function __construct(string $imageUrl, int $shoesID)
    {
        $this->imageUrl = $imageUrl;
        $this->shoesID = $shoesID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $waterMarkUrl = public_path('images/logo/watermark-smal-without-bg.png');
        $file = Image::make($this->imageUrl)
//            ->orientate()
//            ->widen(600)
            ->insert($waterMarkUrl, 'top-left', 0, 0)
            ->encode('jpg');
        $basePath = 'images/';
        $relativePath = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $fileName = sha1(microtime(true)) . '.jpg';
        $fullPath = $basePath . $relativePath . $fileName;
        Storage::disk('public')->put($fullPath, $file);
        $item = Shoes::find($this->shoesID);

        if (empty($item->image)) {
            $item->image = [$fullPath];
        } else {
            $images = $item->image;
            $images[] = $fullPath;
            $item->image = $images;
        }
        $item->save();
    }
}
