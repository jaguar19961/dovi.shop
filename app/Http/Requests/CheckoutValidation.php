<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CheckoutValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'post_code' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'password' => 'required_if:create_an_account,true',
        ];
    }

    public function messages()
    {
        return [
            'country.required' => 'Country is required',
            'first_name.required' => 'First Name is required',
            'last_name.required' => 'last Name is required',
            'address.required' => 'Address is required',
            'city.required' => 'City is required',
            'post_code.required' => 'Postal Code is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email is required a valid email',
            'phone.required' => 'Phone is required',
            'phone.numeric' => 'Phone number is allow only numeric characters',
        ];
    }
}
