<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class isClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check() && in_array($request->user()->type, [1,2]) ){
            return $next($request);
        }
        return redirect()->guest('/login_register');
    }
}
