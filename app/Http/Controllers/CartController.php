<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\Shoes;
use App\Models\Specification;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function getCart(Request $request)
    {
//        dd($request->all());
        $products = $request->all();
        $model = array();
        $total = 0;
        $delivery = 0;
        foreach ($products as $key => $product) {
            if ($product['type'] === 1){
                $prod = Product::without(['category', 'transMany', 'rates', 'labels', 'specifications', 'galleries', 'galleryFilter'])
                    ->findOrFail($product['id']);
                $model[$key]['specification'] = Specification::with('parent')->find($product['specification']);
                $model[$key]['color_image'] = ProductGallery::find($product['color']);
                $model[$key]['color'] = $product['color'];
            }

            if ($product['type'] === 2){
                $prod = Shoes::findOrFail($product['id']);
                $cat = $prod->category->translations != null ? $prod->category->translations[app()->getLocale()] : $prod->category->name;
                $color = $prod->color->translations != null ? $prod->color->translations[app()->getLocale()] : $prod->color->name;
                $sku = $prod->sku;
                $name = ' #'.$sku.' '.$cat.' '.$color;
                $model[$key]['specification'] = [];
                $model[$key]['color_image'] = '/storage/'.$prod->image[0];
                $model[$key]['color'] = [];
                $model[$key]['name'] = $name;
            }
            $model[$key]['data'] = $prod;
            $model[$key]['spec'] = $product['specification'];
            $model[$key]['type'] = $product['type'];
            $model[$key]['quantity'] = $product['quantity'];
            $total += $product['quantity'] * $prod->price['price'];
        }

        $total += $delivery;
        return response(['model' => $model, 'total' => $total, 'delivery_price' => $delivery], 200);
    }
}
