<?php

namespace App\Http\Controllers;

use App\Models\Variable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class VariableController extends Controller
{
    protected $model;

    public function __construct(Variable $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $model = $this->model->orderBy('created_at', 'desc')->get();
        return view('auth.pages.variable.index', compact('model'));
    }

    public function create()
    {
        return view('auth.pages.variable.create');
    }

    public function show($id)
    {
        $model = $this->model->findOrFail($id);
        return view('auth.pages.variable.show', compact('model'));
    }

    public function store(Request $request)
    {

        $this->model::create([
            'key' => $this->generateUuidNumber(),
            'name_ro' => $request->get('name_ro'),
            'name_ru' => $request->get('name_ru'),
            'slug' => Str::slug($request->get('name_ro')),
        ]);

        Session::flash('flash_message', 'Successfully Created!');
        return redirect()->back();

    }

    public function update(Request $request)
    {
        $this->model::find($request->get('id'))->update([
            'name_ro' => $request->get('name_ro'),
            'name_ru' => $request->get('name_ru'),
            'slug' => Str::slug($request->get('name_ro')),
        ]);
        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $model->delete();

        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }


    public function generateUuidNumber()
    {
        $number = mt_rand(100000, 999999);
        if ($this->uuidNumberExists($number)) {
            return $this->generateUuidNumber();
        }
        return $number;
    }

    public function uuidNumberExists($number)
    {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return $this->model::where('key', $number)->exists();
    }
}
