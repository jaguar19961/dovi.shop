<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Shoes;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search($search, $category)
    {
        $q = '%' . $search . '%';
        $products = Product::where('slug', 'Like', '?')
            ->orWhere('sku', 'Like', '?')
            ->orderByRaw("
    CASE WHEN slug LIKE ? THEN 1
         WHEN sku LIKE ? THEN 2
         ELSE 3 END
")->setBindings([$q, $q, $q, $q]);
        $products = $products->take(30)->get();

        $shoes = Shoes::where('slug', 'Like', '?')
            ->orWhere('sku', 'Like', '?')
            ->orderByRaw("
    CASE WHEN slug LIKE ? THEN 1
         WHEN sku LIKE ? THEN 2
         ELSE 3 END
")->setBindings([$q, $q, $q, $q]);
        $shoes = $shoes->take(30)->get();
        $products = $products->merge($shoes);
        return response(['status' => 'success', 'result' => $products]);
    }
}
