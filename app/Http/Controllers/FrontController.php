<?php

namespace App\Http\Controllers;

use App\Facades\Meta;
use App\Facades\Meta\MetaFacade;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\Page;
use App\Models\Product;
use App\Models\Shoes;
use App\Models\Variable;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop');
        MetaFacade::set('description', 'Haine, imbracaminte');
        MetaFacade::set('icon', '');
        $categories = Category::where('parent_id', 0)->get();
        $images = Gallery::get();
//        $populars = Product::where('offer_type_populare', 1)->inRandomOrder()->take(10)->get();
        $populars = Product::inRandomOrder()->take(6)->get();
        $vars = Variable::get()->groupBy('key');

        return view('front.pages.index', compact('categories', 'images', 'populars', 'vars'));
    }

    public function catalog(Request $request, $category = null)
    {
        $url = $request->all();
        # Section description
        MetaFacade::set('title', 'DoviShop Catalog');
        MetaFacade::set('description', 'Catalog');

        if (in_array('category_id', $url)) {
            if ($url['category_id'] == null) {
                $category = Category::first()->slug;
                $url['category_id'] = $category;
            }
        }


        $categories = Category::where('parent_id', 0)->get();

        return view('front.pages.catalog', compact('categories', 'category', 'url'));
    }

    public function catalogCategory()
    {
        $categories = Category::where('parent_id', 0)->get();
        return view('front.pages.catalog_categories', compact('categories'));
    }

    public function product(string $slug = null)
    {
        $model = Product::with('category.parent')->where('slug', $slug)->first();
        if (empty($model)){
            return abort('404');
        }
        # Section description
        MetaFacade::set('title', 'DoviShop' . $model->lang ? $model->lang->name : '');
        MetaFacade::set('description', $model->lang ? $model->lang->description : '');
//        $model = Product::where('id', $slug)->first();

        return view('front.pages.product', compact('model'));
    }

    public function shoes(string $slug = null)
    {
        $model = Shoes::where('slug', $slug)->first();

        $cat = $model->category->translations != null ? $model->category->translations[app()->getLocale()] : $model->category->name;
        $color = $model->color->translations != null ? $model->color->translations[app()->getLocale()] : $model->color->name;
        $sku = $model->sku;
        $name = ' #' . $sku . ' ' . $cat . ' ' . $color;
        # Section description
        MetaFacade::set('title', 'DoviShop' . $name);
        MetaFacade::set('description', $name);
        return view('front.pages.product_shoes', compact('model', 'name'));
    }

    public function myAccount()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Account-ul meu');
        MetaFacade::set('description', 'Account-ul meu');
        return view('front.pages.account.my_account');
    }

    public function orders()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Comenzi');
        MetaFacade::set('description', 'Comenzi');
        return view('front.pages.account.orders');
    }

    public function whishlist()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Produse favorite');
        MetaFacade::set('description', 'Favorite');
        return view('front.pages.account.whishlist');
    }

    public function aboutUs()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Despre noi');
        MetaFacade::set('description', 'Despre noi');
        return view('front.pages.about-us');
    }

    public function cart()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Cart');
        MetaFacade::set('description', 'Cart');
        return view('front.pages.cart');
    }

    public function checkout()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Checkout');
        MetaFacade::set('description', 'Checkout');
        return view('front.pages.checkout');
    }

    public function contact()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Contact');
        MetaFacade::set('description', 'Contact');
        return view('front.pages.contact');
    }

    public function userLogin()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Login page');
        MetaFacade::set('description', 'Login page');
        return view('front.pages.login_register');
    }


    public function userRegister()
    {
        # Section description
        MetaFacade::set('title', 'DoviShop Register register');
        MetaFacade::set('description', 'Register page');
        return view('front.pages.register');
    }


    public function thankYouPage($id)
    {
        # Section description
        MetaFacade::set('title', 'DoviShop thank you page #' . $id);
        MetaFacade::set('description', 'Thank you page');
        $order = Invoice::where('id', $id)->first();
        if (!$order) {
            return redirect()->route('index');
        }
        return view('front.pages.thank_you_page', compact('id'));
    }

    public function getPopularSlider(Request $request)
    {
        $items = Shoes::orderBy('created_at', 'desc')->take(10)->get();
        $items1 = Product::with('lang')->orderBy('created_at', 'desc')->take(10)->get();
        $items = $items->merge($items1);
        $items = $items->shuffle()->all();
        return response(['status' => 200, 'items' => $items], 200);
    }

    public function livrare(){
        $model = Page::find(5);
        return view('front.pages.static_page', compact('model'));
    }


}
