<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Specification;
use App\Models\Whishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WhishlistController extends Controller
{

    public function __construct(){

    }

    public function store(Request $request)
    {
        $input = $request->all();
        DB::beginTransaction();
        try {
            Whishlist::create($input);
            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollback();
            throw $exception;
        }

        return response(['status' => 200], 200);
    }

    public function get()
    {
        $products = Whishlist::where('user_id', auth()->user()->id)->get();
        $model = [];
        foreach ($products as $key => $product) {
            $prod = Product::without(['category', 'transMany', 'rates', 'labels', 'specifications', 'galleries', 'galleryFilter'])
                ->findOrFail($product['product_id']);
            $model[$key]['data'] = $prod;
            $model[$key]['whishlist_id'] = $product['id'];
            $model[$key]['specification'] = Specification::with('parent')->find($product['specification']);
        }
        return response(['status' => 200, 'model' => $model], 200);
    }

    public function delete($id)
    {
        $model = Whishlist::findOrFail($id);
        $model->delete();
        return response(['status' => 200], 200);
    }
}
