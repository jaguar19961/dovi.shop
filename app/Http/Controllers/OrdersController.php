<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Specification;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function get()
    {
        $products = Order::where('user_id', auth()->user()->id)->get();
        $model = [];
        foreach ($products as $key => $product) {
            $prod = Product::without(['category', 'transMany', 'rates', 'labels', 'specifications', 'galleries', 'galleryFilter'])
                ->findOrFail($product['product_id']);
            $model[$key]['data'] = $prod;
            $model[$key]['quantity'] = $product['quantity'];
            $model[$key]['total'] = $product['total'];
            $model[$key]['specification'] = Specification::with('parent')->find($product['specification']);
        }
        return response(['status' => 200, 'model' => $model], 200);
    }
}
