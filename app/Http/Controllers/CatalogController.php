<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductSpecification;
use App\Models\Shoes;
use App\Models\ShoesCategory;
use App\Models\ShoesMaterial;
use App\Models\Specification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    protected $model;

    public function __construct(Product $product)
    {
        $this->model = $product;
    }


    public function get(Request $request)
    {
        $maxPrice = 200;
        $current_category = null;
        if ($request->get('category_id') == 'shoes') {
            $products = Shoes::orderBy('created_at', 'desc')
                ->where('size_count', '>', 0);
            $filters = [
                'color' => ['name' => 'Culoare', 'values' => Color::get()],
                'category' => ['name' => 'Categorii', 'values' => ShoesCategory::get()],
                'material' => ['name' => 'Material', 'values' => ShoesMaterial::get()],
                'sizes' => ['name' => 'Marimi', 'values' => [[
                    'id' => 1,
                    'name' => '34',
                    'translations' => []
                ],
                    [
                        'id' => 2,
                        'name' => '35',
                        'translations' => []
                    ],
                    [
                        'id' => 3,
                        'name' => '36',
                        'translations' => []
                    ],
                    [
                        'id' => 4,
                        'name' => '37',
                        'translations' => []
                    ],
                    [
                        'id' => 5,
                        'name' => '38',
                        'translations' => []
                    ],
                    [
                        'id' => 6,
                        'name' => '39',
                        'translations' => []
                    ],
                    [
                        'id' => 7,
                        'name' => '40',
                        'translations' => []
                    ],
                    [
                        'id' => 8,
                        'name' => '41',
                        'translations' => []
                    ]]
                ]
            ];

            $shoes_color = $request->get('shoes_color');
            $shoes_color_ids = explode(',', $shoes_color);
            $shoes_color_ids = array_map('intval', $shoes_color_ids);
            $shoes_color_ids = array_filter($shoes_color_ids);

            $shoes_category = $request->get('shoes_category');
            $shoes_category_ids = explode(',', $shoes_category);
            $shoes_category_ids = array_map('intval', $shoes_category_ids);
            $shoes_category_ids = array_filter($shoes_category_ids);

            $shoes_sizes = $request->get('shoes_sizes');
            $shoes_sizes_ids = explode(',', $shoes_sizes);
            $shoes_sizes_ids = array_map('intval', $shoes_sizes_ids);
            $shoes_sizes_ids = array_filter($shoes_sizes_ids);

            $shoes_material = $request->get('shoes_material');
            $shoes_material_ids = explode(',', $shoes_material);
            $shoes_material_ids = array_map('intval', $shoes_material_ids);
            $shoes_material_ids = array_filter($shoes_material_ids);

            if (!empty($shoes_material_ids)) {
                $products = $products->whereIn('material_id', $shoes_material_ids);
            }

            if (!empty($shoes_color_ids)) {
                $products = $products->whereIn('color_id', $shoes_color_ids);
            }

            if (!empty($shoes_category_ids)) {
                $products = $products->whereIn('category_id', $shoes_category_ids);

            }

            if (!empty($shoes_sizes_ids)) {
                $products = $products->whereHas('sizes', function ($q) use ($shoes_sizes_ids) {
                    $q->whereIn('value', $shoes_sizes_ids);
                });
            }
        } elseif ($request->get('category_id') != 'shoes') {

            $products = Product::without(['lang', 'category', 'rates', 'specifications'])
                ->with('lang')
                ->orderBy('created_at', 'desc');
            if ($request->get('category_id') != 'undefined' || $request->get('category_id') != 0) {
                $category_ids = Category::where('slug', $request->get('category_id'))->first()->childs->pluck('id')->toArray();
                $products = $products->whereIn('category_id', $category_ids);
//                $category_parent_ids = Category::where('slug', $request->get('category_id'))->first();
//                dd($category_parent_ids->parent);
//                if (!empty($category_parent_ids->parent)) {
//                    $category_parent_ids = $category_parent_ids->parent->pluck('id')->toArray();
//                    $products = $products->whereIn('category_id', $category_parent_ids);
//                }

                $current_category = Category::where('slug', $request->get('category_id'))->with('childs')->first();
            }

            if ($request->get('subcategories') != null) {
                $sub_ids = explode(',', $request->get('subcategories'));
                $sub_ids = array_map('intval', $sub_ids);
                $products = $products->whereIn('category_id', $sub_ids);
            }

            //        ############ GENERATE FILTER ###################
            $productsIds = $products->pluck('id');
            $scopeFilters = Specification::whereHas('productFilter', function ($query) use ($productsIds) {
                $query->whereIn('product_id', $productsIds)->with(['parent'])->distinct();
            })->get();
            $scopeFilters = $scopeFilters->groupBy('parent_id');
            $filters = array();
            foreach ($scopeFilters as $key => $filter) {
                $filters[Specification::findOrFail($key)->lang->name] = $filter;
            }

//        ############ END GENERATE FILTER ###############


//        filtrare dupa specificatie
            if ($request->specs != null) {
                $atr_ids = explode(',', $request->specs);
                $atr_ids = array_map('intval', $atr_ids);
                $products = self::filterProducts($products, $atr_ids);
            }
        }

        $products = $products->orderBy('created_at', 'desc')->paginate($request->get('paginate'));

        return response(['status' => 200, 'products' => $products, 'maxprice' => $maxPrice, 'filters' => $filters, 'current_category' => $current_category]);
    }

    public function old(Request $request)
    {
//        $products = Product::get();
//        dd($products);

        if ($request->sort == 'asc' || $request->sort == 'desc') {
            $sort = $request->sort;
            $sorted = Product::select([
                'products.*',
                'product_specifications.price',
                'product_specifications.product_id'
            ])->join('product_specifications', function ($q) {
                $q->on('products.id', '=', 'product_specifications.product_id');
            });
            $sorted = $sorted->orderBy('product_specifications.price', $sort);
            $sorted = $sorted->pluck('id')->toArray();
            $orderedIds = implode(',', $sorted);
        }


        $maxPrice = 200;
        $maxPrice += ProductSpecification::max('price');
        $products = Product::with(['lang', 'productSpecifications'])->where('no_stock', 0);
        if ($request->sort === 'alphabet' || $request->sort == 'asc' || $request->sort == 'desc') {
            $products = $products->orderByRaw(\DB::raw("FIELD(id, " . $orderedIds . " )"));
        }
//        ############ CATEGORY FILTER ###############
        $cat_id = $request->category_id;
        if ($cat_id != 'null') {
            $products = $products->where('category_id', $cat_id);
            $maxPrice = ProductSpecification::whereHas('product', function ($q) use ($cat_id) {
                $q->where('category_id', $cat_id);
            })->max('price');
        }


        if ($request->sort === 'created_at') {
            $products = $products->orderBy($request->sort, 'desc');
        }

//        ############ END CATEGORY FILTER ###############

//        ############ GENERATE FILTER ###################
        $productsIds = $products->pluck('id');
        $scopeFilters = Specification::whereHas('productFilter', function ($query) use ($productsIds) {
            $query->whereIn('product_id', $productsIds)->with(['parent'])->distinct();
        })->get();
        $scopeFilters = $scopeFilters->groupBy('parent_id');
        $filters = array();
        foreach ($scopeFilters as $key => $filter) {
            $filters[Specification::findOrFail($key)->lang->name] = $filter;
        }

//        ############ END GENERATE FILTER ###############

        if ($request->specs != null) {
            $atr_ids = explode(',', $request->specs);
            $atr_ids = array_map('intval', $atr_ids);
            $products = self::filterProducts($products, $atr_ids);
        }

//        ############ END SPECIFICATION  FILTER ###############
        $search = $request->search;
        if ($search != null && $search != 'null') {
            $products = $products->whereHas('transMany', function ($q) use ($search) {
                $q->where('name', 'LIKE', "%{$search}%");
            });

            $products = $products->orWhereHas('productSpecifications', function ($q) use ($search) {
                $q->where('locale_sku', $search);
            });
        }
        $products = $products->paginate($request->get('paginate'));

        return response(['status' => 200, 'products' => $products, 'maxprice' => $maxPrice, 'filters' => $filters]);
    }

    public function filterProducts($products, $arr)
    {
        $groupByParent = Specification::whereIn('id', $arr)->get()->groupBy('parent_id');
        foreach ($groupByParent as $key => $active) {
            $filter = $active->pluck('id')->toArray();
            $products->whereHas('productAttributes', function ($query) use ($filter) {
                $query->whereIn('specification_id', $filter);
            });
        }

        return $products;
    }
}
