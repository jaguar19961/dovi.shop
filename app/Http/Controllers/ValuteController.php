<?php

namespace App\Http\Controllers;

use App\Models\Valute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ValuteController extends Controller
{
    public function set(Request $request)
    {
        $input = $request->get('valute');
        $valutes = Valute::where('code', $input)->first();
        if (!empty($valutes)) {
            Session::put('valute', $input);
        }
        return redirect()->back();
    }
}
