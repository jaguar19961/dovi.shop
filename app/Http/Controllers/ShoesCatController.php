<?php

namespace App\Http\Controllers;

use App\Models\Shoes;
use App\Models\ShoesCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ShoesCatController extends Controller
{

    protected $model;

    public function __construct(ShoesCategory $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $model = $this->model::get();
        return view('auth.pages.shoes-categories.index', compact('model'));
    }


    public function edit($id)
    {
        $model = $this->model::find($id);
        return view('auth.pages.shoes-categories.show', compact('model'));
    }

    public function update(Request $request)
    {
        $model = $this->model::find($request->get('id'));
        $model->name = $request->key;
        $model->translations = ['ro' => $request->get('name_ro'), 'ru' => $request->get('name_ru')];
        $model->save();
        return redirect()->back();
    }

    public function destroy($id)
    {
        $model = $this->model->findOrFail($id);
        $prod = Shoes::where('category_id', $id)->count();
        if ($prod > 0) {
            Session::flash('flash_message', 'This specification have ' . $prod . ' products, deletion not posible!');
            return redirect()->back();
        } else {
            $model->delete();
            Session::flash('flash_message', 'Successfully deleted!');
            return redirect()->back();

        }
    }
}
