<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutValidation;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\ProductSpecification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    public function checkout(CheckoutValidation $request)
    {
        DB::beginTransaction();
        try {
            $invoice = new Invoice();
            $invoice->country = $request->country;
            $invoice->first_name = $request->first_name;
            $invoice->last_name = $request->last_name;
            $invoice->company_name = $request->company_name;
            $invoice->address = $request->address;
            $invoice->city = $request->city;
            $invoice->region = $request->region;
            $invoice->post_code = $request->post_code;
            $invoice->email = $request->email;
            $invoice->phone = $request->phone;
            $invoice->payment = $request->payment;
            $invoice->status = 1;
            $invoice->total = $request->total;
            $invoice->delivery_price = $request->delivery_price;
            $invoice->save();

            foreach ($request->carts as $product) {
                $order = new Order();
                $order->invoice_id = $invoice->id;
                $order->sku = $product['data']['sku'];
                $order->product_id = $product['data']['id'];
                $order->color_id = $product['color'];
                $order->specification = $product['spec'];
                $order->quantity = $product['quantity'];
                $order->price = $product['data']['price']['price'];
                $order->total = $product['quantity'] * $product['data']['price']['price'];
                $order->save();
            }
            DB::commit();
            $invoice_id = $invoice->id;
            $data = [
                'invoice_id' => $invoice_id,
            ];
            Mail::send('mail.notification_admin_product', $data, function ($message) {
                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
                $message->to('info@dovi.shop');
                $message->subject('Dovi Shop - comanda noua');
            });
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }

        return response(['status' => 200, 'invoice_id' => $invoice_id], 200);
    }
}
