<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductGallery;
use App\Models\ProductLabels;
use App\Models\ProductSpecificationPivot;
use App\Models\Specification;
use App\Models\Translation;
use Carbon\Carbon;
use Composer\Util\Zip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use function PHPUnit\Framework\throwException;

class ProductController extends Controller
{
    protected $model;
    protected $model_name;
    protected $translation;
    protected $brand;

    public function __construct(Product $model, Translation $translation, Brand $brand)
    {
        $this->model = $model;
        $this->translation = $translation;
        $this->brand = $brand;
        $this->model_name = 'App\\Models\\Product::class';
    }

    public function store(Request $request, $id = null, $step = null)
    {
        if ($id == 'null') {
            $id = null;
        }
        if ($step == 1) {
            $model = $this->stepOne($request, $id);
            return response(['model' => $model], 200);
        }

        if ($step == 2) {
            $model = $this->stepTwo($request, $id);
            return response(['model' => $model], 200);
        }

        if ($step == 3) {
            $model = $this->stepThree($request, $id);
            return response(['model' => $model], 200);
        }
        return response(422);
    }

    public function stepOne($request, $id)
    {
        $request->validate([
            'price' => 'required|numeric',
            'category_id' => 'required',
        ]);
        $max_sku = Product::where('furnizor_id', $request->get('furnizor_id'))->get()->max('sku_counter');
        if (!is_null($id)) {
            $model = $this->model::findOrFail($id);
            $model->sku_counter = $max_sku;
        } else {
            $model = new $this->model();
            $model->sku_counter = $max_sku + 1;
        }

        $model->price = $request->price;
        $model->price_buing = $request->price_buing;
        $model->description_ro = $request['description']['ro'];
        $model->description_ru = $request['description']['ru'];
        $model->furnizor_id = $request->furnizor_id;
        $model->category_id = $request->category_id;
        $model->offer_type_promotie = $request['offer_type_id']['promotie'];
        $model->offer_type_populare = $request['offer_type_id']['populare'];
        $model->offer_type_new = $request['offer_type_id']['noi'];
        $model->no_stock = $request['offer_type_id']['no_stock'];
        $model->save();

        $labels = ProductLabels::where('product_id', $model->id)->get();
        foreach ($labels as $label) {
            $label->delete();
        }


        foreach ($request->get('labels') as $lab) {
            ProductLabels::create([
                'product_id' => $model->id,
                'label_id' => $lab
            ]);
        }

        if (!empty($request->get('furnizor_id'))) {
            $furnizor = Brand::find($request->get('furnizor_id'));

            //increment counter
            $this->generateSku($model->id, $furnizor);

        }
        return $model->id;
    }

    public function generateSku($id, $furnizor)
    {
        $max_sku = Product::where('furnizor_id', $furnizor->id)->get()->max('sku_counter');
        $max = $max_sku;

        $prod = Product::find($id);
        //reset sku
        $saveSku = $this->model::findOrFail($id);
        $saveSku->sku = $this->uniqSku($furnizor->code, $prod->sku_counter);
        $saveSku->save();

    }

    public function uniqSku($code, $max)
    {
        $number = str_pad($max, 4, "0", STR_PAD_LEFT);
        return $code . '-' . $number;
    }

    public function stepTwo($request, $id)
    {
        $old = ProductSpecificationPivot::where('product_id', $id)->get();
        foreach ($old as $item) {
            $item->forceDelete();
        }

        //salvare specificatii
        $input = $request->get('specification');
        foreach ($input as $item) {
            ProductSpecificationPivot::create([
                'product_id' => $id,
                'attribute_id' => Specification::with('parent')->find($item)->parent_id,
                'specification_id' => $item,
            ]);
        }


        $model = [
            'model_id' => $id,
            'specification' => ProductSpecificationPivot::where('product_id', $id)
                ->with('attribute.parent')
                ->get()
                ->groupBy('attribute.parent_id'),
        ];

        $this->stepThree($id);

        return $model;
    }

    public function stepThree($id)
    {

        $prod = Product::find($id);
        $prod->slug = Str::slug($prod->category->transMany->where('lang_id', 'ro')->first()->description . '-' . $prod->category->parent->transMany->where('lang_id', 'ro')->first()->description . '-' . $prod->sku);
        $prod->save();

        foreach ($prod->transMany as $trans) {
            $trans->delete();
        }
        //save date options
        $langs = ['ro', 'ru'];
        foreach ($langs as $key => $item) {
            $arr = array(
                'article_id' => $id,
                'name' => self::name($item, $prod),
                'description' => self::description($item),
                'lang_id' => $item,
                'model_name' => $this->model_name,
            );
            $this->translation->create($arr);
        }
    }

    protected function name($lang, $prod)
    {
        return $prod->category->transMany->where('lang_id', $lang)->first()->description . ' ' . $prod->sku;
    }

    protected function description($lang)
    {
        if ($lang == 'ro') {
            return '<span>Acest produs este disponibil la comandă și precomanda. <br></span>' .
                '<span><strong>Termen de livrare:</strong> 3 - 6 zile.<br></span>' .
                '<span>Achitarea se efectuează la primirea comenzii (cash sau card)<br></span>' .
                '<span>Livrarea este gratuită pe tot teritoriul Republicii Moldova prin curier sau poștă.<br></span>' .
                '<span>Mai multe detalii despre livrare aici : <a href="http://dovi.shop/livrare/">https://dovi.shop/livrare/</a></span>';

        }
        if ($lang == 'ru') {
            return '<span>Этот товар доступен для заказа и предзаказа.<br></span>' .
                '<span><strong>Срок доставки:</strong>с 3 до 6 дней.<br></span>' .
                '<span>Оплата производится при получении заказа (наличными или картой).<br></span>' .
                '<span>Доставка осуществляется бесплатно по всей Республике Молдова курьером или почтой.<br></span>' .
                '<span>Подробнее о доставке здесь: <a href="http://dovi.shop/livrare/">https://dovi.shop/livrare/</a></span>';
        }

        return null;
    }


    /**
     * @param Request $request
     * @param $product_id
     * @param $index
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */

    public function uploadGallery(Request $request, $product_id)
    {
        if ($request->hasFile('file')) {
            $product = Product::find($product_id);
            $new = new ProductGallery();
            $dir = '/images/products/';
            if ($request->hasFile('file')) {
                $filename = Carbon::now()->microsecond . '.' . $request->file('file')->getClientOriginalExtension();
                $path = public_path($dir . $filename);
                $waterMarkUrl = public_path('images/logo/watermark-smal-without-bg.png');
                $img = Image::make($request->file('file'), $path);
                $img->resize(750, 1000, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $width = $img->width();
                $height = $img->height();
                $img->insert($waterMarkUrl, 'top-left', 0, 0);
                $img->text($product->sku, 15, $height - 10, function ($font) {
                    $font->file(public_path('fonts/KronaOne-Regular.ttf'));
                    $font->size(28);
                    $font->color('#000000');
                    $font->align('left');
                    $font->valign('bottom');
                })->save($path);

                $new->name = $filename;
                $new->url = $dir . $filename;
                $new->product_id = $product_id;
                $new->save();
            }


            $fileList = ProductGallery::where('product_id', $product_id)->get();
            return response(['status' => 200, 'fileList' => $fileList]);
        }

    }

    public function updateGallery(Request $request)
    {
        $inputs = $request->all();
        foreach ($inputs as $input) {
            $prod = ProductGallery::findOrFail($input['id']);
            $prod->checked = $input['checked'];
            $prod->update();
        }
        return response(['status' => 200]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function delUploadGallery(Request $request)
    {
        $gal = ProductGallery::findOrFail($request->id);
        $path = public_path('/images' . '/products' . '/' . $request->name);
        File::delete($path);
        $gal->delete();

        return response(['status' => 200]);
    }

    public function getGallery($product_id)
    {
        $fileList = ProductGallery::where('product_id', $product_id)->get();
        return response(['status' => 200, 'fileList' => $fileList]);
    }

    public function downloadImage($product_id)
    {
        $product = Product::find($product_id);
        $files = ProductGallery::where('product_id', $product_id)->get();
        // Define Dir Folder
        $public_dir = public_path();
        // Zip File Name
        $zipFileName = $product->sku . '.zip';
        // Create ZipArchive Obj
        $zip = new \ZipArchive();
        if ($zip->open($public_dir . '/' . $zipFileName, \ZipArchive::CREATE) === TRUE) {
            // Add File in ZipArchive
            foreach ($files as $name => $file) {
                $zip->addFile(public_path($file->url), $file->name);
            }
            // Close ZipArchive
            $zip->close();
        }
        // Set Header
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath = $public_dir . '/' . $zipFileName;
        // Create Download Response
        if (file_exists($filetopath)) {
            return response()->download($filetopath, $zipFileName, $headers);
        }
    }

}
