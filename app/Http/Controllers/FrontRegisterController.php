<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FrontRegisterController extends Controller
{
    public function login(LoginRequest $request)
    {
        $attributes = $request->only(['email', 'password']);
        if (Auth::attempt($attributes)) {
            return redirect()->route('account.myAccount');
        }

        return redirect()->back();
    }

    public function register(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|between:8,255',
            'terms' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $user = new User();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = bcrypt($request->password);
            $user->type = 2;
            $user->save();
            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollback();
            throw $exception;
        }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->route('account.myAccount');
        }

        return redirect()->route('account.myAccount');
    }

    public function updateAccount(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|numeric',
            'password' => 'confirmed|between:8,255',
        ]);

        DB::beginTransaction();
        try {
            $user = User::findOrFail(auth()->user()->id);
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->phone = $request->phone;
            if (!empty($request->password)) {
                $user->password = bcrypt($request->password);
            }
            $user->update();
            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollback();
            throw $exception;
        }

        return redirect()->back()->with(['message' => 'Account updated']);
    }
}
