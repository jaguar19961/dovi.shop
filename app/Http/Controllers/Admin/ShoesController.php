<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\ShoesImport;
use App\Jobs\DownloadImages;
use App\Models\Brand;
use App\Models\Shoes;
use App\Models\ShoesSize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class ShoesController extends Controller
{
    protected $model;

    public function __construct(Shoes $shoes)
    {
        $this->model = $shoes;
    }

    public function out()
    {
        $model = $this->model->has('sizes', 0)->paginate(20);
        $brands = Brand::get();
        return view('auth.pages.import.shoes.index', compact('model', 'brands'));
    }

    public function index()
    {
        $model = $this->model->paginate(20);
        $brands = Brand::get();
        return view('auth.pages.import.shoes.index', compact('model', 'brands'));
    }

    public function create()
    {
        $manufacturer = Brand::get();
        return view('auth.pages.import.shoes.create', compact('manufacturer'));
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'file' => 'required',
            'furnizor_id' => 'required|numeric|min:0|not_in:0',
        ]);
        $fileName = md5(now()) . '.' . $request->file('file')->getClientOriginalExtension();
        Storage::disk('public')->put('excel/' . $fileName, $request->file('file')->get());
        Artisan::call('backup:import', ['filename' => $fileName, 'furnizor_id' => $request->get('furnizor_id')]);
//        dispatch(new DownloadImages());
        return redirect()->back();
    }

    public function downloadImage($product_id)
    {
        $product = Shoes::find($product_id);
        $files = $product->image;
        // Define Dir Folder
        $public_dir = public_path();
        // Zip File Name
        $zipFileName = $product->sku . '.zip';
        // Create ZipArchive Obj
        $zip = new \ZipArchive();
        if ($zip->open($public_dir . '/' . $zipFileName, \ZipArchive::CREATE) === TRUE) {
            // Add File in ZipArchive
            foreach ($files as $name => $file) {
                $zip->addFile(Storage::disk('public')->path($file), $name . '.jpg');
            }
            // Close ZipArchive
            $zip->close();
        }
        // Set Header
        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath = $public_dir . '/' . $zipFileName;
        // Create Download Response
        if (file_exists($filetopath)) {
            return response()->download($filetopath, $zipFileName, $headers);
        }
    }

    public function destroy($id)
    {
        $shoe = Shoes::findOrFail($id);
        $size = ShoesSize::where('shoes_id', $id)->get();

        //delete images
        if (is_array($shoe->image) > 0) {
            foreach ($shoe->image as $item) {
                if (Storage::exists('/' . $item)) {
                    Storage::delete('/' . $item);
                }
            }
        }

        //delete size
        foreach ($size as $it) {
            $it->delete();
        }

        //delete mdel
        $shoe->delete();
        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }

    public function deleteall()
    {
        $shoes = Shoes::get();

        //delete images
        foreach ($shoes as $shoe) {
            if (is_array($shoe->image) > 0) {
                foreach ($shoe->image as $item) {
                    if (Storage::exists('/' . $item)) {
                        Storage::delete('/' . $item);
                    }
                }
            }
        }

        //delete size
        DB::table('shoes_sizes')->truncate();
        //delete mdel
        DB::table('shoeses')->truncate();
        Session::flash('flash_message', 'Successfully deleted!');
        return redirect()->back();
    }

    public function updateImages($id)
    {
        $shoes = Shoes::find($id);
        if (empty($shoes)) {
            Session::flash('flash_message', 'Product not found!');
            return redirect()->back();
        }
        if (!empty($shoes->image)) {
            foreach ($shoes->image as $item) {
                if (Storage::exists('/' . $item)) {
                    Storage::delete('/' . $item);
                }
            }
        }

        $shoes->image = null;
        $shoes->save();

        if ($shoes->remote_images != null) {
            collect($shoes->remote_images)->each(function ($image) use ($id) {
                $this->updateImage($id, $image);
            });
        }


        Session::flash('flash_message', 'Successfully updated images!');
        return redirect()->back();
    }

    public function updateImage($id, $img_url)
    {
        $waterMarkUrl = public_path('images/logo/watermark-smal-without-bg.png');
        $file = Image::make($img_url)
//            ->orientate()
//            ->widen(600)
            ->insert($waterMarkUrl, 'top-left', 0, 0)
            ->encode('jpg');
        $basePath = 'images/';
        $relativePath = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $fileName = sha1(microtime(true)) . '.jpg';
        $fullPath = $basePath . $relativePath . $fileName;
        Storage::disk('public')->put($fullPath, $file);
        $item = Shoes::find($id);

        if (empty($item->image)) {
            $item->image = [$fullPath];
        } else {
            $images = $item->image;
            $images[] = $fullPath;
            $item->image = $images;
        }
        $item->save();
    }
}
