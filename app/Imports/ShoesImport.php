<?php

namespace App\Imports;

use App\Models\Color;
use App\Models\Shoes;
use App\Models\ShoesCategory;
use App\Models\ShoesInterior;
use App\Models\ShoesMaterial;
use App\Models\ShoesSize;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Spatie\Async\Pool;

class ShoesImport implements
    ToCollection,
    WithHeadingRow,
    WithStartRow,
    WithChunkReading,
    WithBatchInserts,
    WithMultipleSheets,
    WithUpserts,
    ShouldQueue
{
    use Importable;

    protected $furnizor_id;

    public function __construct($furnizor_id)
    {
        $this->furnizor_id = $furnizor_id;
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        DB::table('shoes_sizes')->truncate();
        foreach ($rows as $row) {
            $db = Shoes::where('sku', !empty($row['sku']) ? $row['sku'] : null)->first();
            if (!empty($db)) {
                $new = Shoes::where('sku', $db->sku)->first();
            } else {
                $new = new Shoes();
            }
            $new->slug = Str::slug($row['sku'] . ' ' . $row['category'] . ' ' . $row['color']);
            $new->price = !empty($row['price']) ? $row['price'] : null;
            $new->valute_id = $row['valute'];
            $new->specification_description = $row['specification_description'];
            $new->remote_images = explode(',', str_replace(PHP_EOL, '', $row['galleries']));
            $new->category_id = $this->checkCategory(strtolower($row['category']));
            $new->color_id = $this->checkColor(strtolower($row['color']));
            $new->material_id = $this->checkMaterial(strtolower($row['material']));
            $new->interior_id = $this->checkInterior(strtolower($row['interior']));
            $new->description_optional_ro = $row['description_optional_ro'];
            $new->description_optional_ru = $row['description_optional_ru'];
            $new->sku = !empty($row['sku']) ? $row['sku'] : null;
            $new->size = $this->setSize($row);
            $new->size_count = count($this->setSize($row));
            $new->furnizor_id = $this->furnizor_id;
            $new->save();

            $this->setSizeDb($row, $new->id);
        }
        Artisan::call('download:images');
    }


    /**
     * @param $row
     */
    public function setSize($row)
    {
        $result = [];
        $sizes = [
            $row['size_1'],
            $row['size_2'],
            $row['size_3'],
            $row['size_4'],
            $row['size_5'],
            $row['size_6'],
            $row['size_7'],
            $row['size_8'],
        ];
        foreach ($sizes as $size) {
            if (!empty($size) && $size != null && $size != " ") {
                if (strlen($size) <= 2) {
                    $result[] = $size;
                }
            }
        }
        return $result;
    }

    public function setSizeDb($row, $id)
    {
        $sizes = [
            $row['size_1'],
            $row['size_2'],
            $row['size_3'],
            $row['size_4'],
            $row['size_5'],
            $row['size_6'],
            $row['size_7'],
            $row['size_8'],
        ];
        $shoes_sizes_ids = array_filter($sizes);
        foreach ($shoes_sizes_ids as $size) {
            if (!empty($size) && $size != null && $size != " ") {
                if (strlen($size) <= 2) {
                    ShoesSize::create([
                        'shoes_id' => $id,
                        'value' => $size
                    ]);
                }
            }
        }
    }

    /**
     * @param $color
     * @return mixed
     */
    public function checkColor($color)
    {
        $item = Color::where('name', $color)->first();
        if (!empty($item)) {
            return $item->id;
        } else {
            $item = new Color();
            $item->name = $color;
            $item->save();
            return (int)$item->id;
        }
    }

    /**
     * @param $category
     * @return mixed
     */
    public function checkCategory($category)
    {
        $item = ShoesCategory::where('name', $category)->first();
        if (!empty($item)) {
            return $item->id;
        } else {
            $item = new ShoesCategory();
            $item->name = $category;
            $item->save();
            return (int)$item->id;
        }
    }


    public function checkMaterial($material)
    {
        $item = ShoesMaterial::where('name', $material)->first();
        if (!empty($item)) {
            return $item->id;
        } else {
            $item = new ShoesMaterial();
            $item->name = $material;
            $item->save();
            return (int)$item->id;
        }
    }

    public function checkInterior($interior)
    {
        $item = ShoesInterior::where('name', $interior)->first();
        if (!empty($item)) {
            return $item->id;
        } else {
            $item = new ShoesInterior();
            $item->name = $interior;
            $item->save();
            return (int)$item->id;
        }
    }

    /**
     * @return int
     */

    public function startRow(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * @return ShoesImport[]
     */
    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }

    /**
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'size';
    }

}
