<?php

use App\Models\Valute;
use \App\Models\Variable;

if (!function_exists('valutes')) {
    function valutes()
    {
        return Valute::get();
    }
}

if (!function_exists('vars')) {
    function vars($key)
    {
        $var = Variable::where('key', $key)->first()['name_' . app()->getLocale()];
        if (!empty($var)) {
            if (auth()->check() && auth()->user()->type === 1) {
                return $var . '[key:' . $key . ']';
            } else {
                return $var;
            }

        }
        return 'Not disponible translation (key:' . $key . ')';
    }
}
