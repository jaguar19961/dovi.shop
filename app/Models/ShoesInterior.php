<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoesInterior extends Model
{
    use HasFactory;

    protected $table = 'shoes_interiors';

    protected $casts = ['translations' => 'array'];
}
