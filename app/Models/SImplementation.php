<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class SImplementation extends Pivot
{
    use HasFactory;

    protected $table = 'specification_implementations';

    protected $fillable = ['parent_id', 'implementation_id'];

    public $timestamps = false;

}
