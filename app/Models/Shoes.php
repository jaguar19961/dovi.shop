<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;

class Shoes extends Model
{
    use HasFactory;

    protected $table = 'shoeses';

    protected $model = 'App\\Models\\Shoes::class';

    protected $fillable = [
        'price',
        'valute_id',
        'image',
        'specification_description',
        'remote_images',
        'category_id',
        'color_id',
        'material_id',
        'interior_id',
        'description_optional_ro',
        'description_optional_ru',
        'sku',
        'size',
        'size_count',
        'seazon_id'];

    protected $with = ['category', 'color', 'sizes', 'rates', 'interior', 'material'];

    protected $casts = ['image' => 'array', 'remote_images' => 'array', 'size' => 'array'];

    protected $appends = ['name', 'type', 'description'];

    public function getAdoptImagesAttribute()
    {
        $images = $this->image;
        $result = [];
        foreach ($images as $image) {
//            $waterMarkUrl = public_path('/images/logo/logo_white.svg');
            $waterMarkUrl = public_path('/images/logo-home-primarbg@2x.png');
//            $ima = Storage::disk('public')->get($image);
            $ima = Image::make(Storage::disk('public')->get($image))->encode('jpg', 50);
            $ima->insert($waterMarkUrl, 'bottom-left', 10, 10)->stream();
//            dd($ima->stream());
//            $ima = response($ima, 200)->header('Content-Type', 'image/jpeg');
//            dd($ima);
//            $img = Image::make($ima->file('file'));
//            dd($img);
//            $img->insert($waterMarkUrl, 'bottom-left', 5, 5);
//            dd($img);
//            $img = Image::make($ima)
//                ->orientate()
//                ->widen(600)->encode('jpg')
//                ->insert($waterMarkUrl, 'bottom-left', 10, 10)->save();
//            dd($img);


//            dd($ima->get);
//            $img = Image::make($ima)->resize(320, 240)->insert($waterMarkUrl);
//dd($img);
//            $img = Image::make($ima);
//            $img->insert($waterMarkUrl, 'bottom-left', 5, 5);
//            $img->stream();
            $result[] = $ima;
        }

        return $result;
    }

    public function category()
    {
        return $this->hasOne(ShoesCategory::class, 'id', 'category_id');
    }

    public function color()
    {
        return $this->hasOne(Color::class, 'id', 'color_id');
    }

    public function interior()
    {
        return $this->hasOne(ShoesInterior::class, 'id', 'interior_id');
    }

    public function material()
    {
        return $this->hasOne(ShoesMaterial::class, 'id', 'material_id');
    }

    public function getPriceAttribute($value)
    {
        $curr_valute = Session::get('valute');
        if (!empty($curr_valute)){
            $valute = Valute::where('code', $curr_valute)->first();
            $res['price'] = round($valute->course->last()->value * $value, 2);
            $res['valute'] = $valute->name;
            return $res;
        }

        return $value;
    }

    public function sizes()
    {
        return $this->hasMany(ShoesSize::class, 'shoes_id', 'id');
    }

    public function getNameAttribute()
    {
        $cat = $this->category->translations != null ? $this->category->translations[app()->getLocale()] : $this->category->name;
        if (!empty($this->category->singular)){
            $color = $this->color->singular != null ? $this->color->singular[app()->getLocale()] : $this->color->name;
        }else{
            $color = $this->color->translations != null ? $this->color->translations[app()->getLocale()] : $this->color->name;
        }


        $sku = $this->sku;
        $name = ' #' . $sku . ' ' . $cat . ' ' . $color;
        return $name;
    }

    public function getTypeAttribute()
    {
        return 'shoes';
    }

    public function rates()
    {
        return $this->hasMany(Rate::class, 'product_id', 'id')->where('model_name', $this->model);
    }

    public function getDescriptionAttribute()
    {
        if (app()->getLocale() == 'ro') {
            return '<span>Acest produs este disponibil la comandă și precomanda. <br></span>' .
                '<span><strong>Termen de livrare:</strong> 7 - 14 zile.<br></span>' .
                '<span>Achitarea se efectuează la primirea comenzii (cash sau card)<br></span>' .
                '<span>Livrarea este gratuită pe tot teritoriul Republicii Moldova prin curier sau poștă.<br></span>' .
                '<span>Mai multe detalii despre livrare aici : <a href="http://dovi.shop/livrare/">https://dovi.shop/livrare/</a></span>';

        }
        if (app()->getLocale() == 'ru') {
            return '<span>Этот товар доступен для заказа и предзаказа.<br></span>' .
                '<span><strong>Срок доставки:</strong> 7 до 14 дней.<br></span>' .
                '<span>Оплата производится при получении заказа (наличными или картой).<br></span>' .
                '<span>Доставка осуществляется бесплатно по всей Республике Молдова курьером или почтой.<br></span>' .
                '<span>Подробнее о доставке здесь: <a href="http://dovi.shop/livrare/">https://dovi.shop/livrare/</a></span>';
        }

        return null;
    }
}
