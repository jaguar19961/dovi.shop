<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'orders';
    protected $model = 'App\\Models\\Order::class';
    protected $fillable = ['invoice_id', 'sku', 'product_id', 'color_id', 'specification', 'quantity', 'price', 'total'];
    protected $with = ['product', 'productShoes'];
    protected $casts = ['specification' => 'array'];

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function productShoes()
    {
        return $this->hasOne(Shoes::class, 'id', 'product_id');
    }
}
