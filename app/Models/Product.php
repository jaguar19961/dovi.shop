<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Session;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['slug', 'name', 'category_id', 'price', 'price_buing', 'offer_type_promotie', 'offer_type_populare', 'offer_type_new',
        'no_stock', 'furnizor_id', 'description_ro', 'description_ru', 'sku'];

    protected $table = 'products';

    protected $model = 'App\\Models\\Product::class';

    protected $with = ['lang', 'category', 'transMany', 'rates', 'labels', 'specifications', 'galleries', 'galleryFilter', 'image'];

    protected $appends = ['calc_rates', 'pluck_label', 'grouped_specs', 'pluck_specs', 'grouped_specification', 'type', 'description'];

    protected $casts = ['offer_type_promotie' => 'boolean', 'offer_type_populare' => 'boolean', 'offer_type_new' => 'boolean', 'no_stock' => 'boolean'];

    public function getPriceAttribute($value)
    {
        $curr_valute = Session::get('valute');
        if (!empty($curr_valute)){
            $valute = Valute::where('code', $curr_valute)->first();
            $res['price'] = round($valute->course->last()->value * $value, 2);
            $res['valute'] = $valute->name;
            return $res;
        }

       return $value;
    }

    public function getDescriptionAttribute()
    {
       return $this['description_'.app()->getLocale()];
    }

    public function galleries()
    {
        return $this->hasMany(ProductGallery::class, 'product_id', 'id');
    }

    public function galleryFilter()
    {
        return $this->hasMany(ProductGallery::class, 'product_id', 'id')
            ->where('checked', 1);
    }

    public function image()
    {
        return $this->hasOne(ProductGallery::class, 'product_id', 'id');
    }

    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')->where('model_name', $this->model)->where('lang_id', app()->getLocale());
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')->where('model_name', $this->model);
    }

    public function productSpecifications()
    {
        return $this->hasMany(ProductSpecification::class, 'product_id', 'id');
    }

    public function productSpecificationsAsc()
    {
        return $this->hasMany(ProductSpecification::class, 'product_id', 'id')->orderBy('price', 'asc');
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductSpecificationPivot::class, 'product_id', 'id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function rates()
    {
        return $this->hasMany(Rate::class, 'product_id', 'id')->where('model_name', $this->model);
    }

    public function furnizor()
    {
        return $this->hasOne(Brand::class, 'id', 'furnizor_id');
    }

    public function labels()
    {
        return $this->hasMany(ProductLabels::class, 'product_id', 'id');
    }

    public function specifications()
    {
        return $this->belongsToMany(
            Specification::class,
            'product_specification_pivot',
            'product_id',
            'specification_id'
        )->withPivot(['spec_id', 'attribute_id'])
            ->using(ProductSpecificationPivot::class);
    }

    public function groupedSpecsAttrib()
    {
        return $this->specifications();
    }

    public function GetPluckLabelAttribute()
    {
        return $this->labels()->pluck('label_id')->toArray();
    }

    public function GetPluckSpecsAttribute()
    {
        return $this->specifications()->pluck('specification_id')->toArray();
    }

    public function GetCalcRatesAttribute()
    {
        $rates = $this->rates();
        $count = $rates->count();
        if ($count > 0) {
            $sum = $rates->sum('rate');
            $total = $sum / $count;
            return $total;
        } else {
            return 0;
        }
    }

    public function getGroupedSpecsAttribute()
    {
        return $this->specifications()->where('show', 1)->with('parent.lang')->get()
            ->sortByDesc('order_by')
            ->unique(function ($spec) {
                return $spec->id;
            })
            ->groupBy(function ($spec) {
                return $spec->parent->lang->name;
            });
    }

    public function spec()
    {
        return $this->hasMany(ProductSpecificationPivot::class, 'product_id', 'id');
    }

    public function getGroupedSpecificationAttribute()
    {
        $res = [];
         $data = $this->spec;
         foreach ($data as $item) {
             $res[$item->attribute_id][] = $item->specification_id;
         }
         return $res;
    }

    public function getTypeAttribute(){
        return 1;
    }
}
