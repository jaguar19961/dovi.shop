<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoesCategory extends Model
{
    use HasFactory;

    protected $table = 'shoes_categories';

    protected $casts = ['translations' => 'array'];
}
