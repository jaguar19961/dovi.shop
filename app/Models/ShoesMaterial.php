<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoesMaterial extends Model
{
    use HasFactory;

    protected $table = 'shoes_materials';

    protected $casts = ['translations' => 'array'];
}
