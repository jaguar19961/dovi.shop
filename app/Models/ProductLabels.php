<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductLabels extends Model
{
    use HasFactory;

    protected $table = 'product_labels';

    protected $model = 'App\\Models\\ProductLabels::class';

    protected $fillable = [
        'id',
        'label_id',
        'product_id',
    ];

    protected $with = ['label'];

    public function label()
    {
        return $this->hasOne(Labels::class, 'id', 'label_id');
    }
}
