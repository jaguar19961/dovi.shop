<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    use HasFactory;

    protected $table = 'variables';
    protected $fillable = ['slug', 'key', 'name_ro', 'name_ru'];

    public function getNameAttribute()
    {
        return $this['name_' . app()->getLocale()];
    }
}
