<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Valute extends Model
{
    use HasFactory;

    protected $table = 'valutes';

    protected $with = ['course'];

    public function course()
    {
        return $this->hasMany(ValuteCourse::class, 'valute_id', 'id');
    }
}
