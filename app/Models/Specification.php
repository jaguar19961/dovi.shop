<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Specification extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'slug', 'parent_id', 'image', 'hex'];

    protected $table = 'specifications';

    protected $model = 'App\\Models\\Specification::class';

    protected $with = ['lang', 'childs'];

    protected $appends = ['name'];

    public function lang()
    {
        return $this->hasOne(Translation::class, 'article_id')->where('model_name', $this->model)->where('lang_id', app()->getLocale());
    }

    public function getNameAttribute()
    {
        return $this->lang->name;
    }

    public function transMany()
    {
        return $this->hasMany(Translation::class, 'article_id')->where('model_name', $this->model);
    }

    public function parent()
    {
        return $this->hasOne(self::class, 'id', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function productFilter()
    {
        return $this->hasMany(ProductSpecificationPivot::class, 'specification_id', 'id');
    }

    public function implemantations()
    {
        return $this->belongsToMany(
            self::class,
            'specification_implementations',
            'parent_id',
            'implementation_id',
            'id',
            'id',

        )->using(SImplementation::class);
    }
}
