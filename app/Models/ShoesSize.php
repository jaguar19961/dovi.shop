<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoesSize extends Model
{
    use HasFactory;

    protected $table = 'shoes_sizes';
    protected $fillable = ['shoes_id', 'value'];
}
