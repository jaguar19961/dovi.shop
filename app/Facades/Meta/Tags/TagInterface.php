<?php
namespace App\Facades\Meta\Tags;

interface TagInterface
{
    public static function tagDefault($key, $value);
}
