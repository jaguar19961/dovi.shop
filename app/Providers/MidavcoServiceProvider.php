<?php

namespace App\Providers;

use App\Facades\Meta\Meta;
use Illuminate\Support\ServiceProvider;

class MidavcoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('meta',function(){
            return new Meta();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
