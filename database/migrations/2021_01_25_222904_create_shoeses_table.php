<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShoesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoeses', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 191)->nullable();
            $table->double('price')->nullable();
            $table->integer('valute_id')->nullable();
            $table->text('image')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('color_id')->nullable();
            $table->integer('sku')->nullable();
            $table->json('size')->nullable();
            $table->integer('seazon_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoeses');
    }
}
