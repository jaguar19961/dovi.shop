<?php

use App\Models\Product;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\GalleryController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SpecificationController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\Admin\RateController;
use App\Http\Controllers\Admin\CreditController;
use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Controllers\Admin\LabelController;
use App\Http\Controllers\Admin\AboutUsController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\Admin\LocalsController;
use App\Http\Controllers\Admin\ScriptController;
use App\Http\Controllers\Admin\ShoesController;
use App\Http\Controllers\FrontController;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

//FRONT
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\ValuteController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\WhishlistController;
use App\Http\Controllers\OrdersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/whishlist-get', [WhishlistController::class, 'get']);
Route::get('/orders-get', [OrdersController::class, 'get']);


Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'valutes']
], function () {
    Route::get('/', [FrontController::class, 'index'])->name('index');
    Route::get('/catalog', [FrontController::class, 'catalogCategory'])->name('catalogCategory');
    Route::get('/catalog/{category}', [FrontController::class, 'catalog'])->name('catalog');
    Route::get('/product/{slug}', [FrontController::class, 'product'])->name('product');
    Route::get('/shoes/{slug}', [FrontController::class, 'shoes'])->name('product.shoes');

    Route::group(['prefix' => 'account',
        'as' => 'account.',
        'middleware' => ['isClient', 'auth', 'web']], function () {
        Route::get('/my_account', [FrontController::class, 'myAccount'])->name('myAccount');
        Route::get('/orders', [FrontController::class, 'orders'])->name('orders');
        Route::get('/whishlist', [FrontController::class, 'whishlist'])->name('whishlist');
    });

    Route::get('/about-us', [FrontController::class, 'aboutUs'])->name('aboutUs');
    Route::get('/livrare', [FrontController::class, 'livrare'])->name('livrare');
    Route::get('/cart', [FrontController::class, 'cart'])->name('cart');
    Route::get('/checkout', [FrontController::class, 'checkout'])->name('checkout');
    Route::get('/contact', [FrontController::class, 'contact'])->name('contact');
    Route::get('/user_register', [FrontController::class, 'userRegister'])->name('userRegister');
    Route::get('/user_login', [FrontController::class, 'userLogin'])->name('userLogin');
    Route::get('/thank-you-page/{invoice_id}', [FrontController::class, 'thankYouPage'])->name('thankYouPage');
});

Route::post('/login-front', [\App\Http\Controllers\FrontRegisterController::class, 'login'])->name('loginfront');
Route::post('/register-front', [\App\Http\Controllers\FrontRegisterController::class, 'register'])->name('registerFront');
Route::post('/register-front-update', [\App\Http\Controllers\FrontRegisterController::class, 'updateAccount'])->name('registerFrontUpdate');

Route::post('/set-valute', [ValuteController::class, 'set'])->name('valute');

Route::group(['prefix' => 'axios', 'as' => 'axios'], function () {
    Route::get('products', [CatalogController::class, 'get']);
    Route::get('/get-global-search/{search}/{category}', [SearchController::class, 'search']);

//    ADMIN
    Route::post('/category/store-atributes', [CategoryController::class, 'storeAtributes'])->name('storeatributes');
    Route::post('/products/specs', [ProductsController::class, 'specs'])->name('products.specs');
    Route::post('/products/product_gallery/{product_id}/{index}', [ProductsController::class, 'uploadGallery'])->name('products.uploadGallery');
    Route::post('/products/del_product_gallery', [ProductsController::class, 'delUploadGallery'])->name('products.delUploadGallery');
    Route::post('/products/get-cat', [ProductsController::class, 'getCat'])->name('products.getCat');
    Route::post('/products/get-gallery/{prod_id}/{index}', [ProductsController::class, 'getGallery'])->name('products.getGallery');
});

//---------- ADMIN ROUTES -----------//

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
], function () {
    Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/dashboard', [AdminController::class, 'index'])->name('home');

        //--------- BLOG ---------//
        Route::get('/blog', [BlogController::class, 'index'])->name('blog');
        Route::get('/blog/show/{id}', [BlogController::class, 'show'])->name('blog.show');

        //--------- End BLOG ---------//

        //--------- about ---------//
        Route::get('/about', [AboutUsController::class, 'index'])->name('about');
        Route::get('/about/show/{id}', [AboutUsController::class, 'show'])->name('about.show');

        //--------- End about ---------//

        //--------- BLOG ---------//
        Route::get('/pages', [PagesController::class, 'index'])->name('pages');
        Route::get('/pages/show/{id}', [PagesController::class, 'show'])->name('pages.show');

        //--------- End BLOG ---------//

        //--------- Testimonials -------//
        Route::get('/testimonial', [TestimonialController::class, 'index'])->name('testimonials');
        Route::get('/testimonials/show/{id}', [TestimonialController::class, 'show'])->name('testimonials.show');
        //--------- END Testimonials -------//

        //--------- Gallery -------//
        Route::get('/gallery', [GalleryController::class, 'index'])->name('gallery');
        Route::get('/gallery/show/{id}', [GalleryController::class, 'show'])->name('gallery.show');
        //--------- END Gallery -------//

        //--------- Category -------//
        Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
            Route::get('/', [CategoryController::class, 'base'])->name('base');
            Route::get('/all/{id}', [CategoryController::class, 'index'])->name('index');
            Route::get('/show/{id}', [CategoryController::class, 'show'])->name('show');
            Route::post('/store', [CategoryController::class, 'store'])->name('store');
            Route::get('/create', [CategoryController::class, 'create'])->name('create');
            Route::post('/update', [CategoryController::class, 'update'])->name('update');
            Route::post('/edit', [CategoryController::class, 'edit'])->name('edit.page');
            Route::get('/destroy/{id}', [CategoryController::class, 'destroy'])->name('destroy');
        });
        //--------- END Category -------//

        //--------- labels -------//
        Route::group(['prefix' => 'label', 'as' => 'label.'], function () {
            Route::get('/', [LabelController::class, 'index'])->name('index');
            Route::get('/show/{id}', [LabelController::class, 'show'])->name('show');
            Route::post('/store', [LabelController::class, 'store'])->name('store');
            Route::get('/create', [LabelController::class, 'create'])->name('create');
            Route::post('/update', [LabelController::class, 'update'])->name('update');
            Route::post('/edit', [LabelController::class, 'edit'])->name('edit.page');
            Route::get('/destroy/{id}', [LabelController::class, 'destroy'])->name('destroy');
        });
        //--------- END labels -------//


        //-----------SCRIPTs----------//
        Route::group(['prefix' => 'script', 'as' => 'script.'], function () {
            Route::get('/', [ScriptController::class, 'index'])->name('index');
            Route::post('/store', [ScriptController::class, 'store'])->name('store');
            Route::get('/show/{id}', [ScriptController::class, 'show'])->name('show');
        });

        //--------- labels -------//
        Route::group(['prefix' => 'locals', 'as' => 'locals.'], function () {
            Route::get('/', [LocalsController::class, 'index'])->name('index');
            Route::get('/show/{id}', [LocalsController::class, 'show'])->name('show');
            Route::post('/store', [LocalsController::class, 'store'])->name('store');
            Route::get('/create', [LocalsController::class, 'create'])->name('create');
            Route::post('/update', [LocalsController::class, 'update'])->name('update');
            Route::post('/edit', [LocalsController::class, 'edit'])->name('edit.page');
            Route::get('/destroy/{id}', [LocalsController::class, 'destroy'])->name('destroy');
        });
        //--------- END labels -------//

        //--------- Specification -------//
        Route::group(['prefix' => 'specification', 'as' => 'specification.'], function () {
            Route::get('/', [SpecificationController::class, 'base'])->name('base');
            Route::get('/all/{id}', [SpecificationController::class, 'index'])->name('get');
            Route::get('/show/{id}', [SpecificationController::class, 'show'])->name('show');
            Route::post('/store', [SpecificationController::class, 'store'])->name('store');
            Route::get('/create', [SpecificationController::class, 'create'])->name('create');
            Route::post('/update', [SpecificationController::class, 'update'])->name('update');
            Route::post('/edit', [SpecificationController::class, 'edit'])->name('page');
            Route::get('/destroy/{id}', [SpecificationController::class, 'destroy'])->name('destroy');
        });

        //--------- END Specification -------//

        //--------- Brand -------//
        Route::get('/brand', [BrandController::class, 'index'])->name('brand');
        Route::get('/brand/show/{id}', [BrandController::class, 'show'])->name('brand.show');
        //--------- END Brand -------//

        //--------- Brand -------//
        Route::get('/variable', [\App\Http\Controllers\VariableController::class, 'index'])->name('variable');
        Route::get('/variable/show/{id}', [\App\Http\Controllers\VariableController::class, 'show'])->name('variable.show');
        //--------- END Brand -------//

        //--------- Products -------//
        Route::get('/products', [ProductsController::class, 'index'])->name('products');
        Route::get('/products/show/{id}', [ProductsController::class, 'show'])->name('products.show');

        //--------- END Products -------//

        //--------- credit ---------//
        Route::get('/credit', [CreditController::class, 'index'])->name('credit');
        Route::get('/credit/show/{id}', [CreditController::class, 'show'])->name('credit.show');
        //--------- End credit ---------//

        //--------- invoice ---------//
        Route::get('/invoice', [InvoiceController::class, 'index'])->name('invoice');
        Route::get('/invoice/show/{id}', [InvoiceController::class, 'show'])->name('invoice.show');
        //--------- End invoice ---------//

        //----------- RATE ----------//


        //----------RATE NED -------==---//
    });
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::post('/blog/store', [BlogController::class, 'store'])->name('blog.store');
    Route::get('/blog/create', [BlogController::class, 'create'])->name('blog.create');
    Route::post('/blog/update', [BlogController::class, 'update'])->name('blog.update');
    Route::post('/blog/edit', [BlogController::class, 'edit'])->name('blog.edit.page');
    Route::get('/blog/destroy/{id}', [BlogController::class, 'destroy'])->name('blog.destroy');

    Route::post('/about/store', [AboutUsController::class, 'store'])->name('about.store');
    Route::get('/about/create', [AboutUsController::class, 'create'])->name('about.create');
    Route::post('/about/update', [AboutUsController::class, 'update'])->name('about.update');
    Route::post('/about/edit', [AboutUsController::class, 'edit'])->name('about.edit.page');
    Route::get('/about/destroy/{id}', [AboutUsController::class, 'destroy'])->name('about.destroy');

    Route::post('/pages/store', [PagesController::class, 'store'])->name('pages.store');
    Route::get('/pages/create', [PagesController::class, 'create'])->name('pages.create');
    Route::post('/pages/update', [PagesController::class, 'update'])->name('pages.update');
    Route::post('/pages/edit', [PagesController::class, 'edit'])->name('pages.edit.page');
    Route::get('/pages/destroy/{id}', [PagesController::class, 'destroy'])->name('pages.destroy');

    Route::post('/testimonials/store', [TestimonialController::class, 'store'])->name('testimonials.store');
    Route::get('/testimonials/state/{id}', [TestimonialController::class, 'state'])->name('testimonials.state');
    Route::get('/testimonials/create', [TestimonialController::class, 'create'])->name('testimonials.create');
    Route::post('/testimonials/update', [TestimonialController::class, 'update'])->name('testimonials.update');
    Route::post('/testimonials/edit', [TestimonialController::class, 'edit'])->name('testimonials.edit.page');
    Route::get('/testimonials/destroy/{id}', [TestimonialController::class, 'destroy'])->name('testimonials.destroy');

    Route::post('/gallery/store', [GalleryController::class, 'store'])->name('gallery.store');
    Route::get('/gallery/create', [GalleryController::class, 'create'])->name('gallery.create');
    Route::post('/gallery/update', [GalleryController::class, 'update'])->name('gallery.update');
    Route::post('/gallery/edit', [GalleryController::class, 'edit'])->name('gallery.edit.page');
    Route::get('/gallery/destroy/{id}', [GalleryController::class, 'destroy'])->name('gallery.destroy');

    Route::post('/brand/store', [BrandController::class, 'store'])->name('brand.store');
    Route::get('/brand/create', [BrandController::class, 'create'])->name('brand.create');
    Route::post('/brand/update', [BrandController::class, 'update'])->name('brand.update');
    Route::post('/brand/edit', [BrandController::class, 'edit'])->name('brand.edit.page');
    Route::get('/brand/destroy/{id}', [BrandController::class, 'destroy'])->name('brand.destroy');

    Route::post('/variable/store', [\App\Http\Controllers\VariableController::class, 'store'])->name('variable.store');
    Route::get('/variable/create', [\App\Http\Controllers\VariableController::class, 'create'])->name('variable.create');
    Route::post('/variable/update', [\App\Http\Controllers\VariableController::class, 'update'])->name('variable.update');
    Route::post('/variable/edit', [\App\Http\Controllers\VariableController::class, 'edit'])->name('variable.edit.page');
    Route::get('/variable/destroy/{id}', [\App\Http\Controllers\VariableController::class, 'destroy'])->name('variable.destroy');

    Route::post('/products/store', [ProductsController::class, 'store'])->name('products.store');
    Route::post('/products/search', [ProductsController::class, 'search'])->name('products.search');
    Route::get('/products/create', [ProductsController::class, 'create'])->name('products.create');
    Route::post('/products/update', [ProductsController::class, 'update'])->name('products.update');
    Route::post('/products/edit', [ProductsController::class, 'edit'])->name('products.edit.page');
    Route::get('/products/destroy/{id}', [ProductsController::class, 'destroy'])->name('products.destroy');
    Route::post('/products/specsGrouped', [ProductsController::class, 'specsGrouped'])->name('products.specsGrouped');

    //new product store//
    Route::post('/new-products/store/{id?}/{step}', [\App\Http\Controllers\ProductController::class, 'store']);
    Route::post('/new-products/product_gallery/{product_id}', [\App\Http\Controllers\ProductController::class, 'uploadGallery'])->name('products.uploadGallery');
    Route::post('/new-products/del_product_gallery', [\App\Http\Controllers\ProductController::class, 'delUploadGallery'])->name('products.delUploadGallery');
    Route::get('/new-products/get-gallery/{product_id}', [\App\Http\Controllers\ProductController::class, 'getGallery'])->name('products.delUploadGallery');
    Route::post('/new-products/updateGallery', [\App\Http\Controllers\ProductController::class, 'updateGallery']);
    Route::get('/new-products/downloadImage/{id}', [\App\Http\Controllers\ProductController::class, 'downloadImage']);


    Route::post('/credit/store', [CreditController::class, 'store'])->name('credit.store');
    Route::get('/credit/create', [CreditController::class, 'create'])->name('credit.create');
    Route::post('/credit/update', [CreditController::class, 'update'])->name('credit.update');
    Route::post('/credit/edit', [CreditController::class, 'edit'])->name('credit.edit.page');
    Route::get('/credit/destroy/{id}', [CreditController::class, 'destroy'])->name('credit.destroy');
    Route::post('/credit/store-credit', [CreditController::class, 'storeCredit'])->name('credit.storecredit');

    Route::post('/invoice/store', [InvoiceController::class, 'store'])->name('invoice.store');
    Route::get('/invoice/create', [InvoiceController::class, 'create'])->name('invoice.create');
    Route::post('/invoice/update', [InvoiceController::class, 'update'])->name('invoice.update');
    Route::post('/invoice/edit', [InvoiceController::class, 'edit'])->name('invoice.edit.page');
    Route::get('/invoice/destroy/{id}', [InvoiceController::class, 'destroy'])->name('invoice.destroy');

    Route::group(['prefix' => 'specification', 'as' => 'specification.'], function () {
        Route::post('/store', [SpecificationController::class, 'store'])->name('store');
        Route::get('/create', [SpecificationController::class, 'create'])->name('create');
        Route::post('/update', [SpecificationController::class, 'update'])->name('update');
        Route::post('/edit', [SpecificationController::class, 'edit'])->name('page');
        Route::get('/destroy/{id}', [SpecificationController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => 'shoes', 'as' => 'shoes.'], function () {
        Route::get('/', [ShoesController::class, 'index'])->name('index');
        Route::get('/create', [ShoesController::class, 'create'])->name('create');
        Route::get('/out', [ShoesController::class, 'out'])->name('out');
        Route::get('/destroy/{id}', [ShoesController::class, 'destroy'])->name('destroy');
        Route::get('/deleteall}', [ShoesController::class, 'deleteall'])->name('deleteall');
        Route::post('/store', [ShoesController::class, 'store'])->name('store');
        Route::get('/downloadImage/{id}', [ShoesController::class, 'downloadImage'])->name('downloadImage');
        Route::get('/update-images/{id}', [ShoesController::class, 'updateImages'])->name('updateImages');
    });

    Route::group(['prefix' => 'shoes-cat', 'as' => 'shoes-cat.'], function () {
        Route::get('/', [\App\Http\Controllers\ShoesCatController::class, 'index'])->name('index');
        Route::get('/edit/{id}', [\App\Http\Controllers\ShoesCatController::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\ShoesCatController::class, 'update'])->name('update');
        Route::get('/destroy/{id}', [\App\Http\Controllers\ShoesCatController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => 'shoes-interior', 'as' => 'shoes-interior.'], function () {
        Route::get('/', [\App\Http\Controllers\ShoesInteriorController::class, 'index'])->name('index');
        Route::get('/edit/{id}', [\App\Http\Controllers\ShoesInteriorController::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\ShoesInteriorController::class, 'update'])->name('update');
        Route::get('/destroy/{id}', [\App\Http\Controllers\ShoesInteriorController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => 'shoes-material', 'as' => 'shoes-material.'], function () {
        Route::get('/', [\App\Http\Controllers\ShoesMaterialController::class, 'index'])->name('index');
        Route::get('/edit/{id}', [\App\Http\Controllers\ShoesMaterialController::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\ShoesMaterialController::class, 'update'])->name('update');
        Route::get('/destroy/{id}', [\App\Http\Controllers\ShoesMaterialController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => 'shoes-colors', 'as' => 'shoes-colors.'], function () {
        Route::get('/', [\App\Http\Controllers\ShoesColorsController::class, 'index'])->name('index');
        Route::get('/edit/{id}', [\App\Http\Controllers\ShoesColorsController::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\ShoesColorsController::class, 'update'])->name('update');
        Route::get('/destroy/{id}', [\App\Http\Controllers\ShoesColorsController::class, 'destroy'])->name('destroy');
    });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
