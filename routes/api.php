<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\WhishlistController;
use App\Http\Controllers\FrontController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/get-cart', [CartController::class, 'getCart']);
Route::post('/checkout', [CheckoutController::class, 'checkout']);
Route::post('/popular-slider', [FrontController::class, 'getPopularSlider']);
Route::get('/products', [\App\Http\Controllers\CatalogController::class, 'get']);

Route::post('/whishlist-store', [WhishlistController::class, 'store']);
Route::get('/whishlist-delete-item/{id}', [WhishlistController::class, 'delete']);


